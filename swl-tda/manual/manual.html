<!DOCTYPE html>
<html lang="en">


<!--
    Users' manual of Test Data Analyzer

    This HTML manual uses Twitter's MIT licensed Bootstrap and the also MIT licensed Simpley Theme by Bootswatch.

    Some of the code was taken from
      * https://jsfiddle.net/KyleMit/v6zhz/89/?utm_source=website&utm_medium=embed&utm_campaign=v6zhz
      * https://jsfiddle.net/a8c9P/
-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Data Analyzer - Users' Handbook</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<header class="navbar navbar-default navbar-fixed-top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Test Data Analyzer - Users' Handbook</a>
        </div>
    </div>
</header>

<div id="overlay"></div>
<div id="overlayContent">
    <img id="imgBig" src=""/>
</div>

<div class="container">
    <div class="row">
        <!--Nav Bar -->
        <nav class="col-xs-3 bs-docs-sidebar">
            <ul id="sidebar" class="nav nav-stacked fixed col-xs-4">
                <li>
                    <a href="#getting-started">Getting Started</a>
                    <ul class="nav nav-stacked">
                        <li><a href="#getting-started-starting">Starting Test Data Analyzer</a></li>
                        <li><a href="#getting-started-loading">Loading Test Runs</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#analyze">Analyzing Data</a>
                    <ul class="nav nav-stacked">
                        <li><a href="#analyzing-problematic">Problematic Classes</a></li>
                        <li><a href="#analyzing-evolution">Evolution</a></li>
                        <li><a href="#analyzing-dependencies">Dependencies</a></li>
                        <li><a href="#analyzing-performance">Performance</a></li>
                        <li><a href="#analyzing-ffc">Frequently Failing Sets</a></li>
                        <li><a href="#analyzing-cra">Classes Requiring Attention</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#customization">Customizing TDA</a>
                </li>

                <li>
                    <a href="#license">License and Authors</a>
                    <ul class="nav nav-stacked">
                        <li><a href="#license-authors">Authors</a></li>
                        <li><a href="#license-license">License</a></li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!--Main Content -->
        <div class="col-xs-9">
            <section id="getting-started" class="group">
                <h1>Getting Started</h1>

                <p>Test Data Analyzer (short TDA) is an analytical software for developers and testers to analyze
                    results of Unit Tests.</p>

                <p>This manual will guide you through using the software.</p>

                <div id="getting-started-starting" class="subgroup">
                    <h2>Starting Test Data Analyzer</h2>

                    <p>To start Test Data Analyzer make sure you have a recent version of Java installed.
                        Test Data Analyzer requires at least Java version 8.</p>
                    <p>Locate <code>tda.jar</code> and simply run it by double clicking it.</p>
                    <p>If you are not on an operating system that allows executing files by double clicking it, open a
                        terminal, navigate relative to <code>tda.jar</code> and run <code>java -jar tda.jar</code>.</p>
                </div>

                <div id="getting-started-loading" class="subgroup">
                    <h2>Loading Test Runs</h2>

                    <p>In order to start using TDA Test Runs have to be loaded. Open some by clicking
                        <b>Load</b> in the <b>Data</b> menu.</p>

                    <p>The opened files appear in the <b>Loaded Test Runs</b> list.</p>

                    <img class="imgSmall" src="img/loaded_test_runs.png" width="600"/>
                </div>

            </section>

            <section id="analyze" class="group">
                <h1>Analyzing Data</h1>

                <div id="analyzing-problematic" class="subgroup">
                    <h2>Problematic Classes</h2>

                    <p>The classes with a high percentage of failed Unit Tests are considered problematic.</p>

                    <p>To view the problematic classes select one or more of the Loaded Test Runs and switch to the
                        <b>Problematic Classes</b> tab if not already selected. A table with all Tested Classes
                        contained in the selected Test Runs is shown.</p>

                    <p>The rows of the table are colored according to their failure percentage. Classes with a failure
                        percentage greater than 80% are colored in red, classes with a failure percentage greater than 50%
                        are colored in yellow.</p>

                    <img class="imgSmall" src="img/problematic_classes.png" width="600"/>
                </div>

                <div id="analyzing-evolution" class="group">
                    <h2>Evolution</h2>

                    <p>It is often interesting to view how the failure percentage of a Tested Class evolved over several
                        Test Runs.</p>

                    <p>To view this evolution select one or more of the Loaded Test Runs and switch to the
                        <b>Evolution</b> tab if not already selected. A list with all Tested Classes that are contained
                        in the Test Runs is shown. Select one or more Tested Classes to populate the chart with data.</p>

                    <img class="imgSmall" src="img/evolution.png" width="600"/>

                    <p>The chart will display the Test Runs ordered by execution date on the x-axis and the failure
                        percentage on the y-axis. If multiple Tested Classes are selected, the failure percentage will be
                        aggregated.</p>
                </div>

                <div id="analyzing-dependencies" class="group">
                    <h2>Dependencies</h2>

                    <p>There are usually dependencies between classes of a program. TDA supports analyzing the Test Runs
                        and finding dependencies between the Tested Classes using the Apriori algorithm.</p>

                    <p>To view the dependencies switch to the <b>Dependencies</b> tab if not already selected. Please
                        note that the selection of Test Runs is ignored for this analysis.</p>

                    <p>On the dependencies tab you are shown a matrix of sets of packages. The size and color of the
                        dots show the support and confidence of the rules.</p>
						
					<div class="alert alert-info"><h4><i class="glyphicon glyphicon-info-sign"></i> Support and Confidence</h4>
						<br/>The <b>Support</b> of a dependency is the percentage of the loaded Test Runs in which the 
						Tested Classes failed.<br/>
						The <b>Confidence</b> of a dependency is the percentage describing how often the rule has been
						found to be true.<br/>
						For a more detailed and siency explanation please consult 
						<a href="https://en.wikipedia.org/wiki/Association_rule_learning"> Wikipedia on Association Rule Learning</a>.</div>


                    <img class="imgSmall" src="img/dependencies.png" width="600"/>

                    <p>Hovering over the dots displays detail information such as contained rules (for the package
                        matrix), support and confidence.</p>

                    <p>By clicking on one of the dots you can zoom in and view the rules between the classes contained
                        in the packages.</p>

                    <p class="alert alert-info"><i class="glyphicon glyphicon-info-sign"></i> If the default Apriori
                        parameters don't fit your needs, <a href="#customization">you can change them</a>.</p>


                </div>

                <div id="analyzing-performance" class="group">
                    <h2>Performance</h2>

                    <p>Analyzing the time Unit Tests take to execute will give an approximate information how the underlying
                        classes are performing.</p>

                    <p>To view performance analysis select one or more Loaded Test Runs and switch to the
                        <b>Performance</b> tab if not already selected. A list with all Tested Classes that are
                        contained in the Test Runs is shown. Select one or more Tested Classes to populate the chart
                        with data.</p>

                    <img class="imgSmall" src="img/performance.png" width="600"/>

                    <p>The chart will display the Test Runs ordered by execution date on the x-axis and the duration
                        of the Tested Classes on the y-axis. If multiple Tested Classes are selected, the duration will
                        be aggregated.</p>

                    <p>Hover over the data points of the chart to view additional information about the respective Test
                        Run such as failure percentage or ran Unit Tests.</p>
                </div>

                <div id="analyzing-ffc" class="group">
                    <h2>Frequently Failing Sets</h2>

                    <p>The Tested Classes that fail often together can give you a first indication on the relationships
                        between these classes.</p>

                    <p>To view these sets of Tested Classes switch to the <b>Frequently failing Sets</b> tab. Please
                        note that the selection of Test Runs is ignored for this analysis.</p>

                    <p>A table containing the Sets of Failing Classes and the support percentage will be shown.</p>


                    <img class="imgSmall" src="img/frequently_failing_sets.png" width="600"/>

                    <p>The sets are used for the <a href="#analyzing-dependencies">Dependency analysis</a>. For more
                        information on how the support value is calculated, please refer to there.</p>
                </div>

                <div id="analyzing-cra" class="group">
                    <h2>Classes Requiring Attention</h2>

                    <p>When developing software one can easily be overwhelmed with the problems occurring, so often
                        times it is difficult to choose what to do next. TDA will show you the Tested Classes that fail
                        the longest.</p>

                    <p>To view the failing classes switch to the <b>Classes Requiring Attention</b> tab. A bar chart
                        will be shown.</p>


                    <img class="imgSmall" src="img/requiring_attention.png" width="600"/>

                    <p>The chart will display the name of the Tested Class on the x-axis and the failure streak on the
                        y-axis.</p>
                </div>
            </section>

            <section id="customization" class="group">
                <h1>Customizing TDA</h1>

                <p>It is likely that the need arises to change some parameters of TDA. Especially the parameters of the
                    <b>Apriori algorithm</b> as used in the <a href="#analyzing-dependencies">Dependencies</a>
                    analysis are likely to need some fine tuning.</p>

                <p>To open the <b>Settings</b> of TDA, click on <b>Preferences...</b> in the <b>Data</b> menu. The
                parameters of Apriori may be changed and logging may be turned on and off.</p>
                
                <img class="imgSmall" src="img/settings.png" width="400">
                
                
            </section>

            <section id="license" class="group">
                <h1>License and Authors</h1>
                <div id="license-authors" class="subgroup">
                    <h2>Authors</h2>

                    <p>TDA was developed as a project of the <em>Software Engineering Lab</em> course of the
                        <em>Otto-Friedrich-University Bamberg</em> in winter semester 2016/17.</p>

                    <p>The group developing TDA consists of</p>
                    <ul>
                        <li><a href="mailto:c%68r%69%73%74ian%2d%62%65rn%68%61%72%64%2e%68%69%6c%64@%73%74%75d.%75ni%2db%61%6db%65%72%67%2ed%65">Christian Hild</a></li>
                        <li><a href="mailto:%6eicola%73%2d%65%6d%69l.b%72%75%63%68@s%74u%64.%75n%69%2d%62%61%6dbe%72%67.%64%65">Nicolas Bruch</a></li>
                        <li><a href="mailto:ni%63%6f%6ca%73.%67r%6fss@s%74%75%64.%75%6e%69-b%61%6d%62%65r%67%2ed%65">Nicolas Gross</a></li>
                        <li><a href="mailto:mi%63%68%61e%6c%2eb%6f%77e%73@st%75%64.%75ni%2db%61%6d%62%65r%67%2ed%65">Michael Bowes</a></li>
                        <li><a href="mailto:%66%6c%6f%72%69a%6e%31%2e%62ee%74%7a@s%74%75d%2e%75%6e%69-%62%61%6d%62%65%72g.%64%65">Florian Beetz</a></li>
                    </ul>
                </div>
                <div id="license-license" class="subgroup">
                    <h2>License</h2>

                    <p>TDA is licensed under the MIT license.</p>

                    <pre class="well">Copyright (c) 2016 Florian Beetz, Michael Bowes, Christian Hild, Nicolas Bruch,
Nicolas Gross

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.</pre>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
