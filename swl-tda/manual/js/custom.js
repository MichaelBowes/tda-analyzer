$(".imgSmall").click(function(){
    $("#imgBig").attr("src", $(this).attr("src"));
    $("#overlay").show();
    $("#overlayContent").show();
});

$("#imgBig").click(function(){
    $("#imgBig").attr("src", "");
    $("#overlay").hide();
    $("#overlayContent").hide();
});

$('body').scrollspy({
    target: '.bs-docs-sidebar',
    offset: 40
});