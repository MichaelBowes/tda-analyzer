import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.uni_bamberg.swl.tda.gui.model.ModelTests;
import de.uni_bamberg.swl.tda.logic.TestRunTests;
import de.uni_bamberg.swl.tda.logic.TestedClassTests;
import de.uni_bamberg.swl.tda.logic.UnitTestTests;
import de.uni_bamberg.swl.tda.logic.apriori.AprioriTests;
import de.uni_bamberg.swl.tda.logic.apriori.AssociationRuleGeneratorTests;
import de.uni_bamberg.swl.tda.logic.apriori.ItemSetGeneratorTests;
import de.uni_bamberg.swl.tda.logic.apriori.PackageDependencyGeneratorTests;
import de.uni_bamberg.swl.tda.logic.parser.ParserTests;
import de.uni_bamberg.swl.tda.logic.parser.XMLUtilitiesTest;

/**
 * Test suite for all tests.
 * 
 * @author all
 *
 */

@RunWith(Suite.class)
@SuiteClasses({

		// model
		ModelTests.class,

		// logic model
		UnitTestTests.class, TestedClassTests.class, TestRunTests.class,

		// logic parser
		XMLUtilitiesTest.class, ParserTests.class,

		// logic.apriori
		AprioriTests.class, ItemSetGeneratorTests.class, PackageDependencyGeneratorTests.class,
		AssociationRuleGeneratorTests.class

})
public class AllTests {

}
