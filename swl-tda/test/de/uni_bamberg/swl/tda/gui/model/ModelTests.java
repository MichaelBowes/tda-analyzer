package de.uni_bamberg.swl.tda.gui.model;

import static org.junit.Assert.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.uni_bamberg.swl.tda.gui.DataPoint;
import de.uni_bamberg.swl.tda.logic.Outcome;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestRunResult;
import de.uni_bamberg.swl.tda.logic.TestRunSetting;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.UnitTestResult;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ModelTests {
	private static List<TestRun> testRunList;
	private static ModelTestImpl model;
	private TestRunSetting setting;
	private TestRunResult result;
	private static List<TestedClass> classList;
	private static TestedClass testedClass1;
	private static TestedClass testedClass2;
	private static TestedClass testedClass3;

	@BeforeClass
	public static void initializeTestClass() throws TdaDataModelException {
		UnitTestResult result1 = new UnitTestResult("r01", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest1 = new UnitTest("test1", "unitTest", "storage", "adapter", "codeBase", result1);
		UnitTestResult result2 = new UnitTestResult("r02", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest2 = new UnitTest("test2", "unitTest", "storage", "adapter", "codeBase", result2);
		UnitTestResult result3 = new UnitTestResult("r03", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest3 = new UnitTest("test3", "unitTest", "storage", "adapter", "codeBase", result3);
		UnitTestResult result4 = new UnitTestResult("r04", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest4 = new UnitTest("test4", "unitTest", "storage", "adapter", "codeBase", result4);
		UnitTestResult result5 = new UnitTestResult("r05", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest5 = new UnitTest("test5", "unitTest", "storage", "adapter", "codeBase", result5);
		UnitTestResult result6 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest6 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result6);
		UnitTestResult result7 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest7 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result7);
		List<UnitTest> testList1 = new LinkedList<>();
		List<UnitTest> testList2 = new LinkedList<>();
		List<UnitTest> testList3 = new LinkedList<>();
		testList1.add(unitTest1);
		testList1.add(unitTest2);
		testList1.add(unitTest3);
		testList2.add(unitTest4);
		testList2.add(unitTest5);
		testList2.add(unitTest6);
		testList3.add(unitTest7);
		testedClass1 = new TestedClass("TestedClass1", testList1);
		testedClass2 = new TestedClass("TestedClass2", testList2);
		testedClass3 = new TestedClass("TestedClass3", testList3);
		
		classList = new LinkedList<>();
		classList.add(testedClass1);
		classList.add(testedClass2);
		classList.add(testedClass3);
	}
	
	@Before
	public void initializeTests() throws TdaDataModelException {
		setting = new TestRunSetting("trs1", "Setting", "rundepl", false, "userdepl", "agentRuleName");
		result = new TestRunResult(Outcome.FAILED, "", "", 0, 0, 0, 0, 6, 3, 0, 0, 0, 0, 3, 0, 0, 0, 6, 0);
		testRunList = new ArrayList<>();
		testRunList.add(new TestRun("xmlns", "TR1", "testRun1", "Fred", "2015-11-26T11:38:43.707194632+01:00",
				"2015-11-26T13:38:43.707194632+01:00", "2015-11-26T13:38:40.707194632+01:00",
				"2015-11-26T13:38:42.707194632+01:00", classList, setting, result));
		testRunList.add(new TestRun("xmlns", "TR2", "testRun2", "Fred", "2015-11-26T11:38:43.707194632+01:00",
				"2015-11-26T13:38:43.707194632+01:00", "2015-11-26T13:38:40.707194632+01:00",
				"2015-11-26T13:38:42.707194632+01:00", classList, setting, result));
		testRunList.add(new TestRun("xmlns", "TR3", "testRun3", "Fred", "2015-11-26T11:38:43.707194632+01:00",
				"2015-11-26T13:38:43.707194632+01:00", "2015-11-26T13:38:40.707194632+01:00",
				"2015-11-26T13:38:42.707194632+01:00", classList, setting, result));
		model = new ModelTestImpl();
		ObservableList<TestedClass> tempdata = FXCollections.observableArrayList();
		tempdata.addAll(testRunList.get(0).getClassList());
		model.selectedClassChanged(tempdata);
		model.setSelectedTestRuns(testRunList);
		ObservableList<TestedClass> obsClassList = FXCollections.observableArrayList();
		obsClassList.add(testRunList.get(0).getClassList().get(0));
		model.setSelectedTestedClasses(obsClassList);
		
	}
	
	@Test
	public void getEvolutionChartDataTest(){
		Map<String, Map<TestRun, Double>> chartData = model.getEvolutionChartData();
		assertEquals(chartData.size(),1);
		assertTrue(chartData.keySet().contains(testRunList.get(0).getClassList().get(0).getName()));
		Map<TestRun,Double> data = chartData.get(testRunList.get(0).getClassList().get(0).getName());
		assertNotNull(data);
		
		Double failurepercentage = 0.0;
		for(TestedClass tc : testRunList.get(0).getClassList()){
			failurepercentage += tc.getFailurePercentage();
		}	
		assertNotNull(data.get(testRunList.get(1)));;
		failurepercentage= failurepercentage/testRunList.get(0).getClassList().size();
		assertEquals(data.get(testRunList.get(0)),failurepercentage,0.1);
	}
	
	@Test
	public void getProblematicClassesDataTest(){
		List<ObservableList<String>> resultList = model.getProblematicClassesData();
		assertNotNull(resultList);
		assertNotNull(resultList.get(0));
		
		double testresult=0;
		int count =0;
		for(TestRun testRun : testRunList){
			testresult += testRun.getClassList().get(0).getFailurePercentage();
			count++;
		}
		testresult= testresult/count;
		double result1 = Double.parseDouble(resultList.get(0).get(1));
		assertEquals(result1,testresult,1);
	}
	
	@Test
	public void getUniqueTestedClassesTest(){
		List<TestedClass> data = model.getUniqueTestedClasses();
		assertNotNull(data);
		Boolean duplicate = false;
		for(TestedClass testedClass : data){
			int count = 0;
			for(TestedClass testedclass : data){				
				if(testedClass.equals(testedclass)){
					count ++;
				}
			}
			if(count>=2){
				duplicate = true;
			}	
		}		
		assertFalse(duplicate);
	}
	
	@Test
	public void getPerformanceChartDataTest(){
		List<DataPoint<TestRun, Long>> data = model.getPerformanceChartData(model.getSelectedTestedClasses());
		for(DataPoint point : data){
			Duration duration = Duration.ZERO;
			TestedClass testedClass = model.getSelectedTestedClasses().get(0);	
			for(UnitTest test : testedClass.getTestList()){
				duration = duration.plus(test.getResult().getDuration());
			}
			assertEquals(data.get(0).getY(),duration.toMillis(),0.1);
			assertEquals(data.get(0).getX().getName(),model.getSelectedTestRuns().get(0).getName());
		}	
	}
	
	
	
}
