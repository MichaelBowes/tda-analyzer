package de.uni_bamberg.swl.tda.gui.model;

import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import javafx.collections.ObservableList;

public class ModelTestImpl extends ModelImpl {

	public void setLoadedTestRuns(ObservableList<TestRun> loadedRuns){
		loadedTestRuns.clear();
		loadedTestRuns.addAll(loadedRuns);
	}

	public void setSelectedTestedClasses(ObservableList<TestedClass> testedClasses){
		selectedTestedClass.clear();
		selectedTestedClass.addAll(testedClasses);
	}
}
