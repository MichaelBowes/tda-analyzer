package de.uni_bamberg.swl.tda.logic.apriori;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.uni_bamberg.swl.tda.logic.Outcome;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestRunResult;
import de.uni_bamberg.swl.tda.logic.TestRunSetting;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.UnitTestResult;

/**
 * Tests for the class {@link Apriori}.
 * 
 * @author Nicolas Gross
 *
 */
public class AprioriTests {

	private TestRun testRun;
	private TestRunSetting setting;
	private TestRunResult result;
	private static List<TestedClass> classList;
	private static TestedClass testedClass1;
	private static TestedClass testedClass2;
	private static TestedClass testedClass3;
	private Apriori apriori;

	@BeforeClass
	public static void initializeTestClass() throws TdaDataModelException {
		UnitTestResult result1 = new UnitTestResult("r01", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest1 = new UnitTest("test1", "unitTest", "storage", "adapter", "codeBase", result1);
		UnitTestResult result2 = new UnitTestResult("r02", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest2 = new UnitTest("test2", "unitTest", "storage", "adapter", "codeBase", result2);
		UnitTestResult result3 = new UnitTestResult("r03", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest3 = new UnitTest("test3", "unitTest", "storage", "adapter", "codeBase", result3);
		UnitTestResult result4 = new UnitTestResult("r04", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest4 = new UnitTest("test4", "unitTest", "storage", "adapter", "codeBase", result4);
		UnitTestResult result5 = new UnitTestResult("r05", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest5 = new UnitTest("test5", "unitTest", "storage", "adapter", "codeBase", result5);
		UnitTestResult result6 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest6 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result6);
		UnitTestResult result7 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest7 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result7);
		List<UnitTest> testList1 = new LinkedList<>();
		List<UnitTest> testList2 = new LinkedList<>();
		List<UnitTest> testList3 = new LinkedList<>();
		testList1.add(unitTest1);
		testList1.add(unitTest2);
		testList1.add(unitTest3);
		testList2.add(unitTest4);
		testList2.add(unitTest5);
		testList2.add(unitTest6);
		testList3.add(unitTest7);
		testedClass1 = new TestedClass("TestedClass1", testList1);
		testedClass2 = new TestedClass("TestedClass2", testList2);
		testedClass3 = new TestedClass("TestedClass3", testList3);

		classList = new LinkedList<>();
		classList.add(testedClass1);
		classList.add(testedClass2);
		classList.add(testedClass3);
	}

	@Before
	public void initializeTests() throws TdaDataModelException {
		setting = new TestRunSetting("trs1", "Setting", "rundepl", false, "userdepl", "agentRuleName");
		result = new TestRunResult(Outcome.FAILED, "", "", 0, 0, 0, 0, 6, 3, 0, 0, 0, 0, 3, 0, 0, 0, 6, 0);
		testRun = new TestRun("xmlns", "TR1", "testRun1", "Fred", "2015-11-26T11:38:43.707194632+01:00",
				"2015-11-26T13:38:43.707194632+01:00", "2015-11-26T13:38:40.707194632+01:00",
				"2015-11-26T13:38:42.707194632+01:00", classList, setting, result);
	}

	@Test
	public void classesWithFailPercBelow15AreSortedOut() {
		List<TestRun> testRunList = new LinkedList<>();
		testRunList.add(testRun);
		testRunList.add(testRun);
		apriori = new Apriori(testRunList);
		List<List<TestedClass>> sortedOutClasses = apriori.filterFailurePerc(testRunList);
		List<List<TestedClass>> correctRes = new LinkedList<>();
		List<TestedClass> correctClassesFirstRun = new LinkedList<>();
		correctClassesFirstRun.add(testedClass1);
		correctClassesFirstRun.add(testedClass2);
		correctRes.add(correctClassesFirstRun);
		List<TestedClass> correctClassesSecondRun = new LinkedList<>();
		correctClassesSecondRun.add(testedClass1);
		correctClassesSecondRun.add(testedClass2);
		correctRes.add(correctClassesSecondRun);
		assertEquals(correctRes, sortedOutClasses);
	}
}
