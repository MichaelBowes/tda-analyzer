package de.uni_bamberg.swl.tda.logic.apriori;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.uni_bamberg.swl.tda.logic.Outcome;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.UnitTestResult;

/**
 * Tests for the class {@link ItemSetGenerator}.
 *
 * @author Nicolas Gross
 *
 */

public class ItemSetGeneratorTests {

	private static TestedClass testedClass1;
	private static TestedClass testedClass2;
	private static TestedClass testedClass3;
	private static List<UnitTest> testList1;
	private static List<UnitTest> testList2;
	private static List<UnitTest> testList3;
	private static List<List<TestedClass>> allTransactions;
	private static ItemSetGenerator itemSetGenerator;
	private static List<ItemSet<TestedClass>> itemSets;

	@BeforeClass
	public static void initializeTestClass() throws TdaDataModelException {
		UnitTestResult result1 = new UnitTestResult("r01", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest1 = new UnitTest("test1", "unitTest", "storage", "adapter", "codeBase", result1);
		UnitTestResult result2 = new UnitTestResult("r02", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest2 = new UnitTest("test2", "unitTest", "storage", "adapter", "codeBase", result2);
		UnitTestResult result3 = new UnitTestResult("r03", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest3 = new UnitTest("test3", "unitTest", "storage", "adapter", "codeBase", result3);
		UnitTestResult result4 = new UnitTestResult("r04", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest4 = new UnitTest("test4", "unitTest", "storage", "adapter", "codeBase", result4);
		UnitTestResult result5 = new UnitTestResult("r05", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest5 = new UnitTest("test5", "unitTest", "storage", "adapter", "codeBase", result5);
		UnitTestResult result6 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest6 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result6);
		UnitTestResult result7 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest7 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result7);
		testList1 = new LinkedList<>();
		testList2 = new LinkedList<>();
		testList3 = new LinkedList<>();
		testList1.add(unitTest1);
		testList1.add(unitTest2);
		testList1.add(unitTest3);
		testList2.add(unitTest4);
		testList2.add(unitTest5);
		testList2.add(unitTest6);
		testList3.add(unitTest7);

	}

	@Before
	public void initializeTests() throws TdaDataModelException {
		testedClass1 = new TestedClass("Package1.SubPackage1.TestedClass1", testList1);
		testedClass2 = new TestedClass("Package1.SubPackage1.TestedClass2", testList2);
		testedClass3 = new TestedClass("Package1.SubPackage2.TestedClass3", testList3);

		allTransactions = new LinkedList<>();
		List<TestedClass> classesOfFirstRun = new LinkedList<>();
		classesOfFirstRun.add(testedClass1);
		classesOfFirstRun.add(testedClass2);
		List<TestedClass> classesOfSecondRun = new LinkedList<>();
		classesOfSecondRun.add(testedClass1);
		classesOfSecondRun.add(testedClass2);
		classesOfSecondRun.add(testedClass3);
		allTransactions.add(classesOfFirstRun);
		allTransactions.add(classesOfSecondRun);
		itemSetGenerator = new ItemSetGenerator(allTransactions, 0.5, 99);
		itemSets = itemSetGenerator.generate();
	}

	@Test
	public void allItemSetsAreGenerated() {
		List<Set<TestedClass>> correct = new LinkedList<>();
		Set<TestedClass> tmp;

		tmp = new HashSet<>();
		tmp.add(testedClass1);
		tmp.add(testedClass2);
		correct.add(tmp);
		tmp = new HashSet<>();
		tmp.add(testedClass1);
		tmp.add(testedClass3);
		correct.add(tmp);
		tmp = new HashSet<>();
		tmp.add(testedClass2);
		tmp.add(testedClass3);
		correct.add(tmp);
		tmp = new HashSet<>();
		tmp.add(testedClass1);
		tmp.add(testedClass2);
		tmp.add(testedClass3);
		correct.add(tmp);

		for (ItemSet<TestedClass> itemSet : itemSets) {
			for (Set<TestedClass> set : correct) {
				if (itemSet.equalsSet(set)) {
					correct.remove(set);
					break;
				}
			}
		}
		assertTrue(correct.isEmpty());
	}

	@Test
	public void itemSetsWhichMembersAreNotInSamePackageAreFiltered() {
		itemSetGenerator = new ItemSetGenerator(allTransactions, 0.5, 2);
		itemSets = itemSetGenerator.generate();

		List<Set<TestedClass>> correct = new LinkedList<>();
		Set<TestedClass> tmp;

		tmp = new HashSet<>();
		tmp.add(testedClass1);
		tmp.add(testedClass2);
		correct.add(tmp);

		for (ItemSet<TestedClass> itemSet : itemSets) {
			for (Set<TestedClass> set : correct) {
				if (itemSet.equalsSet(set)) {
					correct.remove(set);
					break;
				}
			}
		}
		assertTrue(correct.isEmpty());
	}

}
