package de.uni_bamberg.swl.tda.logic.apriori;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import de.uni_bamberg.swl.tda.logic.Outcome;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.UnitTestResult;

public class PackageDependencyGeneratorTests {

	private static PackageDependencyGenerator generator;
	private static List<AssociationRule<TestedClass>> rules;

	@BeforeClass
	public static void initializeTestClass() throws TdaDataModelException {
		UnitTestResult result1 = new UnitTestResult("r01", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest1 = new UnitTest("test1", "unitTest", "storage", "adapter", "codeBase", result1);
		UnitTestResult result2 = new UnitTestResult("r02", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest2 = new UnitTest("test2", "unitTest", "storage", "adapter", "codeBase", result2);
		UnitTestResult result3 = new UnitTestResult("r03", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest3 = new UnitTest("test3", "unitTest", "storage", "adapter", "codeBase", result3);
		UnitTestResult result4 = new UnitTestResult("r04", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest4 = new UnitTest("test4", "unitTest", "storage", "adapter", "codeBase", result4);
		UnitTestResult result5 = new UnitTestResult("r05", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest5 = new UnitTest("test5", "unitTest", "storage", "adapter", "codeBase", result5);
		UnitTestResult result6 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.FAILED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest6 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result6);
		UnitTestResult result7 = new UnitTestResult("r06", "computer", "row", "00:00:01",
				"2016-09-21T13:37:43.7071946+02:00", "2016-09-21T13:37:44.7071946+02:00", Outcome.PASSED, "dir", "tlId",
				"type", "", "");
		UnitTest unitTest7 = new UnitTest("test7", "unitTest", "storage", "adapter", "codeBase", result7);
		List<UnitTest> testList1 = new LinkedList<>();
		List<UnitTest> testList2 = new LinkedList<>();
		List<UnitTest> testList3 = new LinkedList<>();
		testList1.add(unitTest1);
		testList1.add(unitTest2);
		testList1.add(unitTest3);
		testList2.add(unitTest4);
		testList2.add(unitTest5);
		testList2.add(unitTest6);
		testList3.add(unitTest7);
		TestedClass testedClass1 = new TestedClass("pkg1.pkg2.TestedClass1", testList1);
		TestedClass testedClass2 = new TestedClass("pkg1.pkg2.pkg3.TestedClass2", testList2);
		TestedClass testedClass3 = new TestedClass("pkg1.pkg4.TestedClass3", testList3);
		LinkedList<TestedClass> classList = new LinkedList<>();
		classList.add(testedClass1);
		classList.add(testedClass2);
		classList.add(testedClass3);

		rules = new LinkedList<>();

		Set<TestedClass> setPrem1 = new HashSet<>();
		setPrem1.add(testedClass1);
		setPrem1.add(testedClass2);
		ItemSet<TestedClass> premise1 = new ItemSetImpl(setPrem1, 1);
		Set<TestedClass> setConcl1 = new HashSet<>();
		setConcl1.add(testedClass3);
		ItemSet<TestedClass> conclusion1 = new ItemSetImpl(setConcl1, 1);
		AssociationRule<TestedClass> rule1 = new AssociationRuleImpl(premise1, conclusion1, 1, 1);
		rules.add(rule1);

		Set<TestedClass> setPrem2 = new HashSet<>();
		setPrem2.add(testedClass1);
		ItemSet<TestedClass> premise2 = new ItemSetImpl(setPrem2, 1);
		Set<TestedClass> setConcl2 = new HashSet<>();
		setConcl2.add(testedClass2);
		setConcl2.add(testedClass3);
		ItemSet<TestedClass> conclusion2 = new ItemSetImpl(setConcl2, 1);
		AssociationRule<TestedClass> rule2 = new AssociationRuleImpl(premise2, conclusion2, 1, 1);
		rules.add(rule2);

		Set<TestedClass> setPrem3 = new HashSet<>();
		setPrem3.add(testedClass1);
		setPrem3.add(testedClass3);
		ItemSet<TestedClass> premise3 = new ItemSetImpl(setPrem3, 1);
		Set<TestedClass> setConcl3 = new HashSet<>();
		setConcl3.add(testedClass2);
		ItemSet<TestedClass> conclusion3 = new ItemSetImpl(setConcl3, 1);
		AssociationRule<TestedClass> rule3 = new AssociationRuleImpl(premise3, conclusion3, 1, 1);
		rules.add(rule3);

		generator = new PackageDependencyGenerator(rules);
	}

	@Test
	public void correctPackageDependenciesAreGenerated() {
		List<PackageDependency> correctDeps = new LinkedList<>();
		List pkgDeps1 = new LinkedList<>();
		pkgDeps1.add(rules.get(0));
		PackageDependency pkgDep1 = new PackageDependencyImpl("pkg1.pkg2", "pkg1.pkg4", pkgDeps1, 1, 1);
		correctDeps.add(pkgDep1);
		List pkgDeps2 = new LinkedList<>();
		pkgDeps2.add(rules.get(1));
		PackageDependency pkgDep2 = new PackageDependencyImpl("pkg1.pkg2", "pkg1", pkgDeps2, 1, 1);
		correctDeps.add(pkgDep2);
		List pkgDeps3 = new LinkedList<>();
		pkgDeps3.add(rules.get(2));
		PackageDependency pkgDep3 = new PackageDependencyImpl("pkg1", "pkg1.pkg2.pkg3", pkgDeps3, 1, 1);
		correctDeps.add(pkgDep3);

		assertEquals(correctDeps, generator.generatePackageDependencies());
	}

}
