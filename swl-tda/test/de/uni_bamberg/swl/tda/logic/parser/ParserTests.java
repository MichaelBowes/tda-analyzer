package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.*;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the parser.
 *
 * @author Florian Beetz
 */
public class ParserTests {

	private static TestRun testRun;

	@BeforeClass
	public static void parseTestRun() throws Exception {
		testRun = Parser.parse(
				ParserTests.class.getResourceAsStream("data/minimal.xml"));
	}

	@Test
	public void testTestRun() {
		assertEquals("http://microsoft.com/schemas/VisualStudio/TeamTest/2010", testRun.getXmlns());
		assertEquals("5CC1F3BD-D157-4A0E-AB22-492F65AF1C21", testRun.getId());
		assertEquals("tfsbuildagent@TFSBUILDBA706 2016-09-21 13:37:43", testRun.getName());
		assertEquals("DOCEXPERT\\tfsbuildagent", testRun.getRunUser());
		assertEquals("2016-09-21T13:37:43.707194600+02:00", testRun.getCreationDate().toString());
		assertEquals("2016-09-21T14:58:03.547322400+02:00", testRun.getFinishDate().toString());
		assertEquals("2016-09-21T13:37:43.707194600+02:00", testRun.getQueuingDate().toString());
		assertEquals("2016-09-21T13:37:43.707194600+02:00", testRun.getStartDate().toString());
	}

	@Test
	public void testTestSettings() {
		assertEquals("799c2d98-f9f3-4369-bee9-825f39ab07c5", testRun.getSetting().getId());
		assertEquals("default", testRun.getSetting().getName());
		assertEquals("tfsbuildagent_TFSBUILDBA706 2016-09-21 13_37_43", testRun.getSetting().getRunDeploymentRoot());
		assertEquals(false, testRun.getSetting().isUseDefaultDeploymentRoot());
		assertEquals("\\Temp\\", testRun.getSetting().getUserDeploymentRoot());
		assertEquals("Execution Agents", testRun.getSetting().getAgentRuleName());
	}

	@Test
	public void testResultSummary() {
		final TestRunResult r = testRun.getResult();
		assertEquals(Outcome.FAILED, r.getOutcome());
		assertEquals(0, r.getAborted());
		assertEquals(0, r.getCompleted());
		assertEquals(0, r.getDisconnected());
		assertEquals(0, r.getError());
		assertEquals(130, r.getExecuted());
		assertEquals(9, r.getFailed());
		assertEquals(0, r.getInProgress());
		assertEquals(0, r.getInconclusive());
		assertEquals(0, r.getNotExecuted());
		assertEquals(0, r.getNotRunnable());
		assertEquals(121, r.getPassed());
		assertEquals(0, r.getPassedButRunAborted());
		assertEquals(0, r.getPending());
		assertEquals(0, r.getTimeout());
		assertEquals(130, r.getTotal());
		assertEquals(0, r.getWarning());
		assertEquals("Test GetTeilnahmeStatusLasttest was skipped in the test run.\n" +
				"            ", r.getStdOut());
	}

	@Test
	public void testTestedClasses() {
		assertEquals(1, testRun.getClassList().size());

		TestedClass testedClass = testRun.getClassList().get(0);
		assertEquals("DMPplus.Common.LockStringHelper", testedClass.getName());
	}

	@Test
	public void testTestMethod() {
		assertEquals(1, testRun.getClassList().get(0).getTestList().size());

		UnitTest test = testRun.getClassList().get(0).getTestList().get(0);
		assertEquals("ddf67b86-4647-14ba-db6e-043c6aa127cd", test.getId());
		assertEquals("TestIsLockedOnSameWorkstationSameContextPrüfung", test.getName());
		assertEquals("Microsoft.VisualStudio.TestTools.TestTypes.Unit.UnitTestAdapter, Microsoft.VisualStudio.QualityTools.Tips.UnitTest.Adapter", test.getAdapterTypeName());
		assertEquals("\\bin\\DMPplus.Common.Tests.dll", test.getCodeBase());
		assertEquals("\\out\\dmpplus.common.tests.dll", test.getStorage());

		UnitTestResult result = test.getResult();
		assertEquals("e66fac3a-27e0-46e6-bb68-76b66409dfde", result.getExecutionId());
		assertEquals("TFSBUILDBA706", result.getComputerName());
		assertEquals("", result.getDataRowInfo());
		assertEquals(198400, result.getDuration().getNano());
		assertEquals("2016-09-21T13:37:52.859949700+02:00", result.getStartTime().toString());
		assertEquals("2016-09-21T13:37:52.860948800+02:00", result.getEndTime().toString());
		assertEquals(Outcome.PASSED, result.getOutcome());
		assertEquals("e66fac3a-27e0-46e6-bb68-76b66409dfde", result.getRelativeResultsDirectory());
		assertEquals("8c84fa94-04c1-424b-9868-57a2d4851a1d", result.getTestListId());
		assertEquals("13cdc9d9-ddb5-4fa4-a97d-d965ccfc6d4b", result.getTestType());
		assertEquals("", result.getStdOut());
		assertEquals("", result.getErrorInfo());
	}

}
