package de.uni_bamberg.swl.tda.gui;

import de.uni_bamberg.swl.tda.logic.TestedClass;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.*;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides utility functionality for the GUI.
 *
 * @author Christian Hild
 * @author Nicolas Gross
 * @author Florian Beetz
 * @author Nicolas Bruch
 */
public class GuiUtil {

	/**
	 * Fancy Alert for exceptions.
	 *
	 * @implNote taken from the internet
	 * @param ex
	 *            the exception, which information is shown in the alert.
	 */
	public static void exceptionAlert(Exception ex) {
		Alert alert = errorAlert("Fatal Error!", "An unexpected error has occurred", ex.getMessage());
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String exceptionText = sw.toString();
		Label label = new Label("The exception stacktrace was:");
		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);
		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);
		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);
		alert.getDialogPane().setExpandableContent(expContent);
		alert.showAndWait();
	}

	/**
	 * Returns an error alert.
	 * @param title title of the alert.
	 * @param header header of the alert.
	 * @param message message of the alert.
	 * @return the alert.
	 */
	public static Alert errorAlert(String title, String header, String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(message);

		return alert;
	}

	/**
	 * Opens a FileChooser Dialog
	 *
	 * @param owner the parent window.
	 *
	 * @return Optional of a nullable.
	 */
	public static Optional<List<File>> choosePathAlert(Window owner) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter(I18N.m("dialog.open.xmlfile"), "*.xml"),
				new FileChooser.ExtensionFilter(I18N.m("dialog.open.allfiles"), "*.*"));
		return Optional.ofNullable(fileChooser.showOpenMultipleDialog(owner));
	}

	/**
	 * Opens a window with a progress bar, showing the progress of a given task.
	 *
	 * @param task
	 *            the task which progress is visualized.
	 * @return the created {@link Stage}.
	 */
	public static Stage showProgressBar(Task<?> task, Scene parentScene) {
		Stage stage = new Stage(StageStyle.UNDECORATED);
		Group root = new Group();
		Scene scene = new Scene(root, 400, 40);

		stage.initModality(Modality.WINDOW_MODAL);
		stage.initOwner(parentScene.getWindow());
		stage.setScene(scene);
		stage.setTitle(I18N.m("window.title.loading"));

		ProgressBar pb = new ProgressBar();
		pb.setMinHeight(20);
		pb.setMinWidth(380);
		pb.progressProperty().bind(task.progressProperty());
		stage.setOnCloseRequest(event -> {
			if (pb.getProgress() == 1) {
				event.consume();
				stage.close();
			} else {
				event.consume();
			}
		});

		VBox vb = new VBox();
		vb.setPadding(new Insets(10));
		vb.setSpacing(5);
		vb.getChildren().add(pb);
		scene.setRoot(vb);
		stage.show();
		return stage;
	}

	/**
	 * Synchronizes two lists. The source list must be an
	 * {@link ObservableList}.
	 *
	 * @param source
	 *            list to sync to the target.
	 * @param target
	 *            target list ot sync to.
	 * @param <T>
	 *            type of the lists.
	 */
	public static <T> void syncLists(ObservableList<T> source, List<T> target) {
		source.addListener((ListChangeListener<T>) c -> {
			while (c.next()) {
				if (c.wasRemoved()) {
					target.removeAll(c.getRemoved());
				}
				if (c.wasAdded()) {
					try {
						target.addAll(c.getAddedSubList());
					} catch (IndexOutOfBoundsException ignored) {
						// rapid change of items, exception can be safely
						// ignored, another change will be fired, since
						// this one apparently is not even valid anymore
					}
				}
			}
		});
	}

	/**
	 * Opens a dialog for confirmation.
	 * 
	 * @param title
	 *            for the confirmation dialog box.
	 * @param header
	 *            for the confirmation dialog box.
	 * @param question
	 *            for the confirmation dialog box.
	 * @return boolean {@code true} if confirmed or {@code false} if it is
	 *         canceled.
	 *
	 */
	public boolean confirmationAlert(String title, String header, String question) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(question);

		ButtonType buttonTypeOk = new ButtonType(I18N.m("dialog.ok"));
		ButtonType buttonTypeCancel = new ButtonType(I18N.m("dialog.cancel"));

		alert.getButtonTypes().setAll(buttonTypeOk, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		return result.map(button -> button == buttonTypeOk).orElse(false);
	}

	/**
	 * Generates the name of a series for a chart.
	 *
	 * <p>Requires {@code testedClasses} to be non-empty. {@code messageLong} to have a single {@code %s} placeholder
	 * and {@code messageShort} to have a single {@code %d} placeholder.
	 *
	 * <p>Will use the long message containing the full name of classes if less than 4 classes are displayed. Will use
	 * the short message containing only the amount of classes if 4 or more classes are displayed.
	 *
	 * @param testedClasses
	 * 				the classes.
	 * @param messageLong
	 * 				the key for the long message.
	 * @param messageShort
	 * 				the key for the short message.
	 * @return the name of the series.
	 * @throws IllegalArgumentException if testedClasses is empty or null.
	 */
	public static String getSeriesNameForChart(List<TestedClass> testedClasses, String messageLong, String messageShort) {
		if (testedClasses == null || testedClasses.size() < 1)
			throw new IllegalArgumentException("testedClasses is empty.");

		String seriesName;
		if (testedClasses.size() == 1) {
			seriesName = testedClasses.get(0).getName();
		} else {
			if (testedClasses.size() < 4) {
				seriesName = I18N.getMessage(messageLong,
						testedClasses.stream().map(TestedClass::getName).collect(Collectors.joining(", ")));
			} else {
				seriesName = I18N.getMessage(messageShort, testedClasses.size());
			}
		}

		return seriesName;
	}

}
