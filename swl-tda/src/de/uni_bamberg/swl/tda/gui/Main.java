package de.uni_bamberg.swl.tda.gui;

import de.uni_bamberg.swl.tda.gui.controller.MainController;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Main entry point for the application.
 *
 * @author Christian Hild
 * @author Florian Beetz
 */
public class Main extends Application {

	private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

	/**
	 * Main entry point for the application.
	 * @param args command line arguments.
	 */
	public static void main(String[] args) {
		setupLogger();
		LOGGER.info("Launching TDA.");
		launch(args);
		LOGGER.info("Exiting.");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		MainController mainController = new MainController();
		mainController.openWindow();
	}

	/**
	 * Returns {@code true} if the debug flag is set.
	 *
	 * @return status of the debug flag.
	 */
	public static boolean isDebug() {
		return Boolean.parseBoolean(System.getProperty("tda.debug", "false"));
	}

	private static void setupLogger() {
		try {
			LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("/logging.properties"));
		} catch (IOException e) {
			System.err.println("Failed to load logging properties, logging may not work as expected.");
			e.printStackTrace();
		}
	}

}
