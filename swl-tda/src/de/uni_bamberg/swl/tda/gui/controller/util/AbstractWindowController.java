package de.uni_bamberg.swl.tda.gui.controller.util;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * Basic controller for windows.
 *
 * @author Florian Beetz
 */
public abstract class AbstractWindowController {

    private final Window owner;
    private final boolean sizeToScene;
    private Parent root;
    private Scene scene;
    private Stage stage;

    private final EventHandler<WindowEvent> closeRequestHandler = event -> {
        onCloseRequest(event);
        event.consume();
    };

    public AbstractWindowController() {
        this(null);
    }

    public AbstractWindowController(Window owner) {
        this(owner, true);
    }

    public AbstractWindowController(Window owner, boolean sizeToScene) {
        this.owner = owner;
        this.sizeToScene = sizeToScene;
    }

    /**
     * Returns the root node of the scene graph.
     * <p>
     * If the node is not yet present it is constructed.
     *
     * @return the root node.
     */
    public Parent getRoot() {
        if (root == null) {
            makeRoot();
            assert root != null;
        }

        return root;
    }

    /**
     * Returns the scene.
     * <p>
     * If the scene does not yet exist it is constructed.
     * <p>
     * <em>Note:</em> May only be called from the FX Thread.
     *
     * @return the scene of this controller.
     */
    public Scene getScene() {
        assert Platform.isFxApplicationThread();

        if (scene == null) {
            scene = new Scene(getRoot());
            controllerDidCreateScene();
        }

        return scene;
    }

    /**
     * Returns the stage.
     * <p>
     * If the stage does not yet exist it is constructed.
     * <p>
     * <em>Note:</em> May only be called from the FX Thread.
     *
     * @return the stage of this controller.
     */
    public Stage getStage() {
        assert Platform.isFxApplicationThread();

        if (stage == null) {
            stage = new Stage();
            stage.initOwner(owner);
            stage.setOnCloseRequest(closeRequestHandler);
            stage.setScene(getScene());
            if (sizeToScene) {
                stage.sizeToScene();
            }

            controllerDidCreateStage();
        }

        return stage;
    }

    /**
     * Opens the window.
     * <p>
     * <em>Note:</em> May only be called from the FX Thread.
     */
    public void openWindow() {
        assert Platform.isFxApplicationThread();

        getStage().show();
        getStage().toFront();
    }

    /**
     * Closes the window.
     * <p>
     * <em>Note:</em> May only be called from the FX Thread.
     */
    public void closeWindow() {
        assert Platform.isFxApplicationThread();

        getStage().close();
    }

    /**
     * Constructs the root node.
     */
    protected abstract void makeRoot();

    /**
     * Notifies the controller that the user requested the window to close.
     *
     * @param event the received window event.
     */
    protected abstract void onCloseRequest(WindowEvent event);

    /**
     * Sets the root of the controller.
     *
     * @param root the node to set as root.
     */
    protected final void setRoot(Parent root) {
        assert root != null;
        this.root = root;
    }

    /**
     * Callback to notify implementors that the scene was created.
     * <p>
     * Overriding methods should call super.
     */
    protected void controllerDidCreateScene() {
        assert getRoot() != null;
        assert getRoot().getScene() != null;
        assert getRoot().getScene().getWindow() == null;
    }

    /**
     * Callback to notify implementors that the stage was created.
     * <p>
     * Overriding methods should call super.
     */
    protected void controllerDidCreateStage() {
        assert getRoot() != null;
        assert getRoot().getScene() != null;
        assert getRoot().getScene().getWindow() != null;
    }
}
