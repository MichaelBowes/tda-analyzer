package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.util.Callback;

/**
 * Handles searching of classes.
 *
 * @author Nicolas Gross
 * @author Florian Beetz
 */
public class ClassSearchController extends AbstractFxmlPanelController {

	@FXML // fx:id="classesListView"
	private ListView<TestedClass> classesListView; // Value injected by
													// FXMLLoader

	@FXML // fx:id="searchTextField"
	private TextField searchTextField; // Value injected by FXMLLoader

	private final ObservableList<TestedClass> classList = FXCollections.observableArrayList();

	protected ClassSearchController(Model model) {
		super(ClassSearchController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/ClassSearchView.fxml"),
				I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {
		if (GuiConstants.SELECTED_TESTRUNS_CHANGED.equals(args)) {
			this.classList.setAll(model.getUniqueTestedClasses());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		assert classesListView != null;
		assert searchTextField != null;

		classesListView.setCellFactory(new Callback<ListView<TestedClass>, ListCell<TestedClass>>() {
			@Override
			public ListCell<TestedClass> call(ListView<TestedClass> param) {
				return new ListCell<TestedClass>() {
					@Override
					protected void updateItem(TestedClass item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null && !empty) {
							setText(item.getName());
						} else {
							setText("");
						}
					}
				};
			}
		});

		classesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
				
		classesListView.getSelectionModel().getSelectedItems().addListener((ListChangeListener<TestedClass>) c -> {				
			ObservableList<TestedClass> selectedItems = classesListView.getSelectionModel().getSelectedItems();
			getModel().selectedClassChanged(selectedItems);
		});
		
		initializeFilteredList();
	}

	/**
	 * Initializes the filtered list.
	 * 
	 * @see <a href=
	 *         "http://code.makery.ch/blog/javafx-8-tableview-sorting-filtering/">Source</a>
	 */
	private void initializeFilteredList() {
		FilteredList<TestedClass> filteredClasses = new FilteredList<>(this.classList, p -> true);

		searchTextField.textProperty()
				.addListener((observable, oldValue, newValue) -> filteredClasses.setPredicate(testedClass -> {
					// If filter text is empty, display all classes.
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}

					// Compare class name with filter and ignore capitalization.
					String filter = newValue.toLowerCase();

					return testedClass.getName().toLowerCase().contains(filter);
				}));

		this.classesListView.setItems(filteredClasses);
	}
}
