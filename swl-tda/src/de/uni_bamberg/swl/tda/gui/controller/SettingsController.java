package de.uni_bamberg.swl.tda.gui.controller;

import java.text.DecimalFormat;

import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlWindowController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.stage.WindowEvent;

/**
 * Controller for the settings dialog.<br>
 * 
 * @author Christian Hild
 */
public class SettingsController extends AbstractFxmlWindowController {

	private static final DecimalFormat df = new DecimalFormat(I18N.m("data.percent.numberFormat"));

	private final Model model;

	@FXML
	private Slider sliderMinConfidence;

	@FXML
	private Label labelMinConfidence;

	@FXML
	private Label labelMinSupport;

	@FXML
	private CheckBox checkboxLogging;

	@FXML
	private Slider sliderMinSupport;

	protected SettingsController(Model model) {
		super(SettingsController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/SettingsView.fxml"),
				I18N.getBundle());

		this.model = model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		this.getStage().setTitle("Settings");
		fillSettingsDialog();
	}

	private void fillSettingsDialog() {
		sliderMinSupport.setValue(model.getDependencyMinSupport());
		labelMinSupport.setText(df.format(sliderMinSupport.getValue()));
		sliderMinSupport.valueProperty().addListener((o, ov, nv) -> labelMinSupport.textProperty()
				.setValue(String.valueOf(df.format(sliderMinSupport.getValue()))));

		sliderMinConfidence.setValue(model.getDependencyMinConfidence());
		labelMinConfidence.setText(df.format(sliderMinConfidence.getValue()));
		sliderMinConfidence.valueProperty().addListener((o, ov, nv) -> labelMinConfidence.textProperty()
				.setValue(String.valueOf(df.format(sliderMinConfidence.getValue()))));
		checkboxLogging.setSelected(model.getLoggingStatus());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCloseRequest(WindowEvent event) {
		if (new GuiUtil().confirmationAlert(I18N.m("dialog.settings.closeConfirmation.title"),
				I18N.m("dialog.settings.closeConfirmation.header"),
				I18N.m("dialog.settings.closeConfirmation.question"))) {
			this.closeWindow();
		}
	}

	/**
	 * Discards all unsaved changes.
	 */
	@FXML
	void discardButtonAction() {
		this.closeWindow();
	}

	/**
	 * Saves the settings.
	 */
	@FXML
	void saveButtonAction() {
		setSettingsInModel();
		this.closeWindow();
	}

	private void setSettingsInModel() {
		model.setDependencyMinSupport(sliderMinSupport.getValue());
		model.setDependencyMinConfidence(sliderMinConfidence.getValue());
		model.setLoggingStatus(checkboxLogging.isSelected());

		model.aprioriParameterChanged();
		model.saveSettings();

		this.closeWindow();

	}

}
