package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractWindowController;
import de.uni_bamberg.swl.tda.gui.controls.dependencies.DependencyMatrix;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.apriori.PackageDependency;
import javafx.stage.WindowEvent;

/**
 * Controller for the detail Association Rules view.
 *
 * @author Florian Beetz
 */
public class AssociationsDetailController extends AbstractWindowController {

	private final AssociationRulesController parent;
	private final Model model;
	private final PackageDependency data;

	public AssociationsDetailController(AssociationRulesController parent, Model model, PackageDependency data) {
		this.parent = parent;
		this.model = model;
		this.data = data;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void makeRoot() {
		getStage().setTitle(I18N.m("dialog.association.detail.title", data.toString()));

		DependencyMatrix<TestedClass> dependencyMatrix = new DependencyMatrix<>();
		dependencyMatrix.getAssociationRules().addAll(data.getAssociationRules());
		dependencyMatrix.setMinSupport(model.getDependencyMinSupport());
		dependencyMatrix.setMinConfidence(model.getDependencyMinConfidence());

		dependencyMatrix.setLongTextFactory(TestedClass::getName);
		dependencyMatrix.setShortTextFactory(testedClass -> {
			if (testedClass == null)
				return null;
			return testedClass.getName().substring(testedClass.getName().lastIndexOf(".") + 1);
		});

		setRoot(dependencyMatrix);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCloseRequest(WindowEvent event) {
		getStage().close();
		parent.childClosed(this);
	}
}
