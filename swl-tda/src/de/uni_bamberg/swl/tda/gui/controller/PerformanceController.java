package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.DataPoint;
import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.controller.util.XYChartController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.Outcome;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tooltip;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the Performance tab of TDA.
 * <p>
 *
 * @author Michael Bowes
 */
public class PerformanceController extends AbstractFxmlPanelController implements XYChartController<String, Long, TestRun, Long> {

	@FXML // fx:id="performanceChart"
	private LineChart<String, Long> performanceChart; // value is injected by the FXMLLoader

	@FXML // fx:id="classNameLabel"
	private Label classNameLabel; // value is injected by the FXMLLoader

	protected PerformanceController(Model model) {
		super(PerformanceController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/PerformanceView.fxml"),
				I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {
		populateChart();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("Duplicates")
	protected void controllerDidLoadFxml() {
		assert performanceChart != null;
		assert classNameLabel != null;

		// construct child controllers
		ClassSearchController classSearchController = new ClassSearchController(getModel());

		// add content
		assert getPanelRoot() instanceof SplitPane;
		SplitPane root = (SplitPane) getPanelRoot();
		root.getItems().add(0, classSearchController.getPanelRoot());

		populateChart();
	}

	/**
	 * Creates a Map of Maps of TestRuns and adds each to the series for the LineChart.
	 */
	private void populateChart() {
		clearChart();

		List<TestedClass> testedClasses = new ArrayList<>(getModel().getSelectedTestedClasses());
		testedClasses.remove(null); // remove null elements

		try {
			String seriesName = GuiUtil.getSeriesNameForChart(testedClasses, "component.performanceView.legend.long",
					"component.performanceView.legend.short");

			addSeries(seriesName, getModel().getPerformanceChartData(testedClasses));
		} catch (IllegalArgumentException e) {
			// ignored, testedClasses is empty, nothing to populate
		}
	}

	@Override
	public void controllerDidAddPoint(DataPoint<TestRun, Long> point, XYChart.Data<String, Long> data) {
		Node node = data.getNode();
		List<TestedClass> testedClasses = getModel().getSelectedTestedClasses(point.getX());

		Tooltip.install(node, new Tooltip(getPerformanceTooltip(testedClasses, point.getY())));
		String color = allClassesPassedInTestRun(point.getX(), testedClasses) ? "green" : "red";
		node.setStyle("-fx-background-color: " + color + ", white !important;");
	}

	private String getPerformanceTooltip(List<TestedClass> testedClasses, long duration) {
		long total = testedClasses.stream()
				.mapToLong(testedClass -> testedClass.getTestList().size())
				.sum();

		int passed = testedClasses.stream()
				.flatMap(testedClass -> testedClass.getTestList().stream())
				.map(unitTest -> unitTest.getResult().getOutcome())
				.mapToInt(outcome -> outcome == Outcome.PASSED ? 1 : 0)
				.sum();

		return I18N.m("component.performanceView.tooltip", total, passed, (total - passed), duration);
	}

	private boolean allClassesPassedInTestRun(TestRun run, List<TestedClass> testedClasses) {
		for (TestedClass identifierClass : testedClasses) {
			try {
				TestedClass testedClass = run.getTestedClass(identifierClass.getName());
				if (testedClass.getFailurePercentage() > 0.15) return false;
			} catch (TdaDataModelException e) {
				// class not present in run, can be ignored
			}
		}
		return true;
	}

	@Override
	public XYChart<String, Long> getChart() {
		return performanceChart;
	}

	@Override
	public String xToView(TestRun testRun) {
		return GuiConstants.DATE_TIME_FORMATTER.format(testRun.getCreationDate());
	}

}
