package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.apriori.ItemSet;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;

/**
 * Controller for the Frequently failing Sets tab.
 *
 * @author Nicolas Gross
 */

public class FrequentlyFailingSetsController extends AbstractFxmlPanelController {

    @FXML // fx:id="frequentSetsTable"
    private TableView<ItemSet<TestedClass>> frequentSetsTable;
    // Value injected by FXMLLoader

    @FXML // fx:id="classNamesColumn"
    private TableColumn<ItemSet<TestedClass>, String> classNamesColumn;
    // Value injected by FXMLLoader

    @FXML // fx:id="setSupportColumn"
    private TableColumn<ItemSet<TestedClass>, Number> setSupportColumn;
    // Value injected by FXMLLoader

    protected FrequentlyFailingSetsController(Model model) {
        super(FrequentlyFailingSetsController.class.getResource(
                "/de/uni_bamberg/swl/tda/gui/view/FrequentlyFailingSetsView.fxml"), I18N.getBundle(), model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void controllerDidLoadFxml() {
        assert frequentSetsTable != null;
        assert classNamesColumn != null;
        assert setSupportColumn != null;
        createTableProperties();
    }

    /**
     * Creates the properties for the {@link TableView}.
     * <p>
     * Each row shows one {@link ItemSet} of {@link TestedClass}.
     * <p>
     * The first {@link TableColumn} shows the names of the
     * {@link TestedClass} contained in the {@link ItemSet}. The second
     * {@link TableColumn} shows the support percentage of the {@link ItemSet}.
     * <p>
     *
     * @see <a href= "https://stackoverflow.com/questions/26220896/showing-tooltips-in-javafx-at-specific-row-position-in-the-tableview">Source</a>
     */
    private void createTableProperties() {
        frequentSetsTable.setRowFactory(tv -> new TableRow<ItemSet<TestedClass>>() {
            Tooltip tooltip = new Tooltip();

            @Override
            public void updateItem(ItemSet<TestedClass> itemSet, boolean empty) {
                super.updateItem(itemSet, empty);
                if (itemSet == null) {
                    setTooltip(null);
                } else {
                    tooltip.setText(itemSet.toString());
                    setTooltip(tooltip);
                }
            }
        });
        classNamesColumn.setCellValueFactory(cell -> new SimpleStringProperty(namesWithoutPackages(cell.getValue())));
        setSupportColumn.setCellValueFactory(
                itemSet -> new SimpleDoubleProperty(Math.round(itemSet.getValue().getSupport() * 10000.0) / 100.0));
        setSupportColumn.setSortType(TableColumn.SortType.DESCENDING);
        frequentSetsTable.getSortOrder().add(setSupportColumn);
    }

    /**
     * Returns a string with all names of the {@code ItemSet}'s
     * {@link TestedClass}, without their package names.
     *
     * @return the string with all names.
     */
    private String namesWithoutPackages(ItemSet<TestedClass> itemSet) {
        if (itemSet.size() == 0) {
            return "{}";
        }
        StringBuffer names = new StringBuffer();
        names.append("{");
        for (TestedClass testedClass : itemSet) {
            String nameParts[] = testedClass.getName().split("\\.");
            names.append(nameParts[nameParts.length - 1] + ", ");
        }
        names.delete(names.length() - 2, names.length());
        names.append("}");
        return names.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onModelChange(Model model, Object args) {
        frequentSetsTable.getItems().setAll(model.getFrequentItemSets());
        frequentSetsTable.getSortOrder().add(setSupportColumn);
    }

}
