package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

/**
 * Controller for the Problematic classes Tab.<br>
 * 
 * @author Nicolas Bruch
 */
public class ProblematicClassesController extends AbstractFxmlPanelController {

	@FXML // fx:id="classNameTable"
	private TableView<ObservableList<String>> classNameTable;
	// Value injected by FXMLLoader

	@FXML // fx:id="failurePercentageColumn"
	private TableColumn<ObservableList<String>, Number> failurePercentageColumn;
	// Value injected by FXMLLoader

	@FXML // fx:id="classNameColumn"
	private TableColumn<ObservableList<String>, String> classNameColumn;
	// Value injected by FXMLLoader

	protected ProblematicClassesController(Model model) {
		super(ProblematicClassesController.class
				.getResource("/de/uni_bamberg/swl/tda/gui/view/ProblematicClassesView.fxml"), I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {
		classNameTable.getItems().clear();
		if (getModel().getSelectedTestRuns() != null) {
			classNameTable.getItems().addAll(getModel().getProblematicClassesData());
		}
		classNameTable.getSortOrder().add(failurePercentageColumn);
		setFailurePercentageCellFactory();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		assert failurePercentageColumn != null;
		assert classNameTable != null;
		assert classNameColumn != null;
		createTableProperties();
	}

	/**
	 * Creates the properties for the {@link TableView}.<br>
	 * The {@code rowValue} on the index 0 is the string containing the class
	 * name.<br>
	 * The {@code rowValue} on the index 1 is the string containing the rounded
	 * double value and is therefore parsed to double to enable sorting.
	 */
	private void createTableProperties() {

		// sets the CellValueFactory for the classNameColumn.
		classNameColumn.setCellValueFactory(cellData -> {
			ObservableList<String> rowValue = cellData.getValue();
			String cellValue = rowValue.get(0);
			return new ReadOnlyStringWrapper(cellValue);
		});
		// sets the CellValueFactory for the failurePercentageColumn by parsing
		// the string to a double.
		failurePercentageColumn.setCellValueFactory(cellData -> {
			ObservableList<String> rowValue = cellData.getValue();
			Double cellValue = Double.parseDouble(rowValue.get(1));
			return new ReadOnlyDoubleWrapper(cellValue);
		});

		setFailurePercentageCellFactory();
		failurePercentageColumn.setSortType(TableColumn.SortType.DESCENDING);
		classNameTable.getSortOrder().add(failurePercentageColumn);
	}

	/**
	 * Sets the color of the {@link #classNameTable} {@link TableRow}s depending
	 * on their failure percentage in the {@link #failurePercentageColumn}.<br>
	 * The row(s) with the highest failure percentage will be colored red (can
	 * be multiple).<br>
	 * The row(s) with a failure percentage above 80 will be colored orange.<br>
	 * The row(s) with a failure percentage above 50 will be colored yellow.<br>
	 */
	private void setFailurePercentageCellFactory() {
		failurePercentageColumn.setCellFactory(column -> new TableCell<ObservableList<String>, Number>() {
			@Override
			protected void updateItem(Number item, boolean empty) {
				super.updateItem(item, empty);
				setText(empty ? "" : getItem().toString());
				setGraphic(null);

				@SuppressWarnings("unchecked")
				TableRow<Double> currentRow = getTableRow();

				double max = getMaxFailurePercentage();
				if (!isEmpty()) {
					double doubleItem = item.doubleValue();
					if (max > 0.0 && doubleItem == max) {
						currentRow.setStyle("-fx-background-color: #FF6060");
					} else if (doubleItem > 80) {
						currentRow.setStyle("-fx-background-color: #FFAD60");
					} else if (doubleItem > 50) {
						currentRow.setStyle("-fx-background-color: #fcfa8d");
					} else {
						currentRow.setStyle("");
					}
				}
			}
		});
	}

	/**
	 * @return Double value containing the highest failure percentage contained
	 *         in the {@link #classNameTable}.
	 */
	private double getMaxFailurePercentage() {
		double max = 0;
		for (ObservableList<String> testedClass : classNameTable.getItems()) {
			if (Double.parseDouble(testedClass.get(1)) > max) {
				max = Double.parseDouble(testedClass.get(1));
			}
		}
		return max;
	}

}
