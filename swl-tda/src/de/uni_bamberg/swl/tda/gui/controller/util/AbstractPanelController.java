package de.uni_bamberg.swl.tda.gui.controller.util;

import de.uni_bamberg.swl.tda.gui.model.Model;

import java.util.Observable;
import java.util.Observer;

import javafx.scene.Parent;

/**
 * Basic controller for panels.
 *
 * @author Florian Beetz
 */
public abstract class AbstractPanelController implements Observer {

    private final Model model;
    private Parent panelRoot;

    protected AbstractPanelController(Model model) {
        assert model != null;
        this.model = model;
        model.addObserver(this);
    }

    /**
     * Returns the model this panel shows data of.
     *
     * @return the model used.
     */
    public Model getModel() {
        return model;
    }

    /**
     * Returns the root node of the scene graph of this panel.
     * <p>
     * If the node does not yet exist it is created.
     *
     * @return the root node of this controller.
     */
    public Parent getPanelRoot() {
        if (panelRoot == null) {
            makePanel();
            assert panelRoot != null;
        }

        return panelRoot;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Model) {
            onModelChange((Model) o, arg);
        }
    }

    /**
     * Constructs the root node.
     */
    protected abstract void makePanel();

    /**
     * Sets the root element of this controller.
     *
     * @param parent the element to set as root.
     */
    protected final void setPanelRoot(Parent parent) {
        assert parent != null;
        panelRoot = parent;
    }

    /**
     * Notifies the controller that the model changed.
     *
     * @param model the model that changed.
     * @param args the arguments that were supplied.
     */
    protected abstract void onModelChange(Model model, Object args);
}
