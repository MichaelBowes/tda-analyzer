package de.uni_bamberg.swl.tda.gui.controller.util;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.stage.Window;

/**
 * Basic controller for windows loaded from FXML files.
 *
 * @author Florian Beetz
 */
public abstract class AbstractFxmlWindowController extends AbstractWindowController {

    private final URL fxmlURL;
    private final ResourceBundle resources;

    public AbstractFxmlWindowController(URL fxmlURL) {
        this(fxmlURL, null);
    }

    public AbstractFxmlWindowController(URL fxmlURL, ResourceBundle resources) {
        this(fxmlURL, resources, true);
    }

    public AbstractFxmlWindowController(URL fxmlURL, ResourceBundle resources, boolean sizeToScene) {
        this(fxmlURL, resources, null, sizeToScene);
    }

    public AbstractFxmlWindowController(URL fxmlURL, ResourceBundle resources, Window owner, boolean sizeToScene) {
        super(owner, sizeToScene);
        assert fxmlURL != null : "Check FXML path given to " + getClass().getSimpleName();
        this.fxmlURL = fxmlURL;
        this.resources = resources;
    }

    /**
     * Returns the {@link URL} to the FXML resource that this controller displays.
     *
     * @return {@link URL} to FXML.
     */
    public URL getFxmlURL() {
        return fxmlURL;
    }

    /**
     * Returns the {@link ResourceBundle} used to internationalize this controller.
     *
     * @return the {@link ResourceBundle}.
     */
    public ResourceBundle getResources() {
        return resources;
    }

    @Override
    protected void makeRoot() {
        final FXMLLoader loader = new FXMLLoader();

        loader.setController(this);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        try {
            setRoot(loader.load());
            controllerDidLoadFxml();
        } catch (IOException e) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), e);
        }
    }

    /**
     * Callback to notify implementors that the the FXML was loaded.
     * <p>
     * Overriding methods should call super.
     */
    protected void controllerDidLoadFxml() {
        assert getRoot() != null;
        assert getRoot().getScene() == null;
    }
}
