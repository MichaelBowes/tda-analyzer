package de.uni_bamberg.swl.tda.gui.controller;

import java.util.List;
import java.util.logging.Logger;

import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.controls.dependencies.DependencyEvent;
import de.uni_bamberg.swl.tda.gui.controls.dependencies.DependencyMatrix;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.apriori.AssociationRule;
import de.uni_bamberg.swl.tda.logic.apriori.PackageDependency;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

/**
 * Handles the "Association Rules" tab.
 *
 * @author FLorian Beetz
 */
public class AssociationRulesController extends AbstractFxmlPanelController {

	private static final Logger LOGGER = Logger.getLogger(AssociationRulesController.class.getName());

	private ObservableList<AssociationsDetailController> openChildWindows = FXCollections.observableArrayList();

	@FXML
	private AnchorPane rootPane;

	private final DependencyMatrix<String> dependencyMatrix = new DependencyMatrix<>();

	protected AssociationRulesController(Model model) {
		super(AssociationRulesController.class
				.getResource("/de/uni_bamberg/swl/tda/gui/view/AssociationRulesView.fxml"), I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {

		if (args == null || GuiConstants.LOADED_TESTRUNS_CHANGED.equals(args)) {

			dependencyMatrix.getAssociationRules().clear();
			dependencyMatrix.setMinSupport(model.getDependencyMinSupport());
			dependencyMatrix.setMinConfidence(model.getDependencyMinConfidence());
			List<PackageDependency> rules = model.getPackageDependencies();
			if (rules != null) {
				dependencyMatrix.getAssociationRules().addAll(rules);
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		assert rootPane != null;

		dependencyMatrix.setEventHandler(this::handleDependencyEvent);
		dependencyMatrix.setRuleDescriptionFactory(this::getDescriptionForRule);

		rootPane.getChildren().add(dependencyMatrix);

		AnchorPane.setTopAnchor(dependencyMatrix, 0d);
		AnchorPane.setBottomAnchor(dependencyMatrix, 0d);
		AnchorPane.setLeftAnchor(dependencyMatrix, 0d);
		AnchorPane.setRightAnchor(dependencyMatrix, 0d);

		// onModelChange(getModel(), null);
	}

	/**
	 * Callback for the child windows to notify when closed.
	 *
	 * @param child
	 *            the child that closed.
	 */
	void childClosed(AssociationsDetailController child) {
		openChildWindows.remove(child);
	}

	/**
	 * Callback for the containing controller to notify when the window is
	 * closed.
	 */
	void onCloseRequest() {
		openChildWindows.forEach(AssociationsDetailController::closeWindow);
	}

	private void handleDependencyEvent(DependencyEvent<String> event) {
		assert event.getRule() instanceof PackageDependency;

		PackageDependency dependency = (PackageDependency) event.getRule();

		if (event.getEventType() == DependencyEvent.CLICKED) {
			LOGGER.fine("Opening detail view.");
			AssociationsDetailController controller = new AssociationsDetailController(this, getModel(), dependency);
			controller.openWindow();
			openChildWindows.add(controller);
		}
	}

	private String getDescriptionForRule(AssociationRule<String> rule) {
		assert rule instanceof PackageDependency;

		PackageDependency packageDependency = (PackageDependency) rule;

		return I18N.m("component.rules.tooltip", packageDependency.getAssociationRules().size(),
				packageDependency.getSupport(), packageDependency.getConfidence());
	}

}
