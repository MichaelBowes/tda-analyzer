package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.Main;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlWindowController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.gui.model.ModelImpl;
import de.uni_bamberg.swl.tda.logic.TestRun;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Handles the main window of TDA.
 *
 * @author Christian Hild
 * @author Florian Beetz
 * @author Michael Bowes
 * @author Nicolas Bruch
 */
@SuppressWarnings("FieldCanBeLocal")
public class MainController extends AbstractFxmlWindowController {

	private final Model model;
	private final GuiUtil guiUtil;
	@FXML // fx:id="frequentlyFailingSetsTab"
	private Tab frequentlyFailingSetsTab; // Value injected by FXMLLoader
	@FXML // fx:id="associationRulesTab"
	private Tab associationRulesTab; // Value injected by FXMLLoader
	@FXML // fx:id="evolutionTab"
	private Tab evolutionTab; // Value injected by FXMLLoader
	@FXML // fx:id="problematicClassesTab"
	private Tab problematicClassesTab; // Value injected by FXMLLoader
	@FXML // fx:id="performanceTab"
	private Tab performanceTab; // Value injected by FXMLLoader
	@FXML // fx:id="testRunList"
	private ListView<TestRun> testRunList; // Value injected by FXMLLoader
	@FXML // fx:id="testRunList"
	private Tab requiringAttentionTab; // Value injected by FXMLLoader
	private ProblematicClassesController problematicClassesController;
	private EvolutionController evolutionController;
	private FrequentlyFailingSetsController frequentlyFailingSetsController;
	private AssociationRulesController associationRulesController;
	private PerformanceController performanceController;
	private RequiringAttentionController requiringAttentionController;

	public MainController() {
		super(MainController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/MainView.fxml"), I18N.getBundle(),
				true);
		guiUtil = new GuiUtil();
		model = new ModelImpl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		assert frequentlyFailingSetsTab != null;
		assert associationRulesTab != null;
		assert evolutionTab != null;
		assert performanceTab != null;
		assert problematicClassesTab != null;
		assert testRunList != null;

		getStage().setTitle(I18N.m("application.name"));
		getStage().setMinHeight(600);
		getStage().setMinWidth(800);

		// setup list view
		testRunList.setCellFactory(new Callback<ListView<TestRun>, ListCell<TestRun>>() {
			@Override
			public ListCell<TestRun> call(ListView<TestRun> param) {
				return new ListCell<TestRun>() {
					@Override
					protected void updateItem(TestRun item, boolean empty) {
						super.updateItem(item, empty);

						ContextMenu contextMenu = new ContextMenu();
						MenuItem deleteItem = new MenuItem();
						deleteItem.textProperty()
								.bind(Bindings.format(I18N.m("window.main.loadedTestRuns.remove"), itemProperty()));
						deleteItem.setOnAction(event -> {
							testRunList.getItems().remove(item);
							model.getSelectedTestRuns().remove(item);
						});
						contextMenu.getItems().addAll(deleteItem);

						if (item != null && !empty) {
							setText(GuiConstants.DATE_TIME_FORMATTER.format(item.getStartDate()));
							setContextMenu(contextMenu);
						} else {
							setText("");
							setContextMenu(null);
						}
					}
				};
			}
		});
		testRunList.setItems(model.getLoadedTestRuns());
		testRunList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		GuiUtil.syncLists(testRunList.getSelectionModel().getSelectedItems(), model.getSelectedTestRuns());

		model.selectedTestRunsProperty()
				.addListener((observable, oldValue, newValue) -> testRunList.getSelectionModel().getSelectedItems());

		// create controllers for panels
		problematicClassesController = new ProblematicClassesController(model);
		evolutionController = new EvolutionController(model);
		frequentlyFailingSetsController = new FrequentlyFailingSetsController(model);
		associationRulesController = new AssociationRulesController(model);
		performanceController = new PerformanceController(model);
		requiringAttentionController = new RequiringAttentionController(model);

		// add contents of tabs
		problematicClassesTab.setContent(problematicClassesController.getPanelRoot());
		evolutionTab.setContent(evolutionController.getPanelRoot());
		frequentlyFailingSetsTab.setContent(frequentlyFailingSetsController.getPanelRoot());
		associationRulesTab.setContent(associationRulesController.getPanelRoot());
		associationRulesTab.getContent().setPickOnBounds(true);
		performanceTab.setContent(performanceController.getPanelRoot());
		requiringAttentionTab.setContent(requiringAttentionController.getPanelRoot());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCloseRequest(WindowEvent event) {
		closeWindow();
	}

	/**
	 * Opens a dialog to choose files for loading.
	 */
	@FXML
	void loadMenuItemAction() {
		Optional<List<File>> fileList = GuiUtil.choosePathAlert(getStage());
		fileList.ifPresent(files -> {
			model.loadXMLFiles(files, getScene());
		});
	}

	/**
	 * Opens the preferences window.
	 */
	@FXML
	void preferencesMenuItemAction() {
		new SettingsController(model).openWindow();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@FXML
	public void closeWindow() {
		Stage stage = getStage();
		if (!model.getLoadedTestRuns().isEmpty() && !Main.isDebug()) {
			if (guiUtil.confirmationAlert(I18N.m("dialog.closeConfirmation.title"),
					I18N.m("dialog.closeConfirmation.header"), I18N.m("dialog.closeConfirmation.question"))) {
				stage.close();
				associationRulesController.onCloseRequest();
			}
		} else {
			stage.close();
			associationRulesController.onCloseRequest();
		}
	}

	/**
	 * Clears all TestRuns loaded into the TDA software.
	 */
	@FXML
	public void clearAllTestRuns() {
		model.resetApriori();
		model.getLoadedTestRuns().clear();
		model.getSelectedTestedClasses();
	}

	/**
	 * Shows the dialogue window with the text about the TDA.
	 */
	@FXML
	public void showAboutDialog() {
		// Source used:
		// http://stackoverflow.com/questions/22166610/how-to-create-a-popup-windows-in-javafx
		// Some basic information about the TDA software.
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(I18N.m("dialog.about.title"));
		alert.setHeaderText(I18N.m("dialog.about.header"));
		alert.setContentText(I18N.m("dialog.about.description"));

		// Title for the drop-down window.
		Label label = new Label(I18N.m("dialog.about.license.label"));

		// Text box with the license in it.
		TextArea textBox = new TextArea(I18N.m("dialog.about.license"));
		textBox.setEditable(false);
		textBox.setWrapText(true);
		textBox.setMaxWidth(Double.MAX_VALUE);
		textBox.setMaxHeight(Double.MAX_VALUE);

		// Drop-down window for the license.
		GridPane LicensePane = new GridPane();
		alert.getDialogPane().setExpandableContent(LicensePane);
		LicensePane.setMaxWidth(Double.MAX_VALUE);
		LicensePane.add(label, 0, 0);
		LicensePane.add(textBox, 0, 1);

		alert.getDialogPane().setExpandableContent(LicensePane);

		// Property adjustment for OS compatibility with Unix systems.
		alert.getDialogPane().expandedProperty().addListener(l -> Platform.runLater(() -> {
			alert.getDialogPane().requestLayout();
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			stage.sizeToScene();
		}));

		alert.showAndWait();

	}

	/**
	 * Opens the manual in the browser.
	 */
	@FXML
	void showManual() {
		File manual = new File("manual/manual.html");

		if (!manual.exists()) {
			GuiUtil.errorAlert(
					I18N.m("dialog.manual.notFound.title"),
					I18N.m("dialog.manual.notFound.header"),
					I18N.m("dialog.manual.notFound.text")
			).show();
		} else {
				if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
					new Thread(() -> {
						try {
							Desktop.getDesktop().browse(manual.toURI());
						} catch (IOException e) {
							Platform.runLater(() -> {
								GuiUtil.exceptionAlert(e);
							});
						}
					}).start();
				} else if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
					new Thread(() -> {
						try {
							Desktop.getDesktop().open(manual.getParentFile());
						} catch (IOException e) {
							Platform.runLater(() -> {
								GuiUtil.exceptionAlert(e);
							});
						}
					}).start();
				} else {
					StringSelection selection = new StringSelection(manual.toURI().toString());
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, selection);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle(I18N.m("dialog.manual.clipboard.title"));
					alert.setContentText(I18N.m("dialog.manual.clipboard.message"));
					alert.show();
				}
		}
	}

}
