package de.uni_bamberg.swl.tda.gui.controller;

import java.util.ArrayList;
import java.util.List;

import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.controller.util.XYChartController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;

/**
 * Handles the Evolution tab of TDA.
 * <p>
 * @author Michael Bowes
 */
public class EvolutionController extends AbstractFxmlPanelController implements XYChartController<String, Double, TestRun, Double> {

	@FXML // fx:id="evolutionChart"
	private LineChart<String, Double> evolutionChart; // value is injected by the FXMLLoader

	@FXML // fx:id="classNameLabel"
	private Label classNameLabel; // value is injected by the FXMLLoader

	protected EvolutionController(Model model) {
		super(EvolutionController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/EvolutionView.fxml"),
				I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {
		populateChart();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("Duplicates")
	protected void controllerDidLoadFxml() {
		assert evolutionChart != null;
		assert classNameLabel != null;

		// construct child controllers
		ClassSearchController classSearchController = new ClassSearchController(getModel());

		// add content
		assert getPanelRoot() instanceof SplitPane;
		SplitPane root = (SplitPane) getPanelRoot();
		root.getItems().add(0, classSearchController.getPanelRoot());

		populateChart();
	}
	
	/**
	 * Populates the chart with the average failure percentage of the selected {@link TestedClass}es over the
	 * {@link TestRun}s.
	 */
	private void populateChart() {
		clearChart();

		List<TestedClass> testedClasses = new ArrayList<>(getModel().getSelectedTestedClasses());
		testedClasses.remove(null); // remove null elements

		try {
			String seriesName = GuiUtil.getSeriesNameForChart(testedClasses, "component.evolutionView.legend.long",
					"component.evolutionView.legend.short");

			addSeries(seriesName, getModel().getEvolutionChartData(testedClasses));
		} catch (IllegalArgumentException e) {
			// ignored, testedClasses is empty, nothing to populate
		}
	}

	@Override
	public XYChart<String, Double> getChart() {
		return evolutionChart;
	}

	@Override
	public String xToView(TestRun testRun) {
		return GuiConstants.DATE_TIME_FORMATTER.format(testRun.getCreationDate());
	}
}
