/**
 * All source files in this package are based on source code of
 * <a href="https://bitbucket.org/gluon-oss/scenebuilder">Scene Builder</a> (licensed under the BSD
 * license).
 *
 * @author Florian Beetz
 */
package de.uni_bamberg.swl.tda.gui.controller.util;