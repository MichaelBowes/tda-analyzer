package de.uni_bamberg.swl.tda.gui.controller;

import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.gui.controller.util.AbstractFxmlPanelController;
import de.uni_bamberg.swl.tda.gui.controller.util.XYChartController;
import de.uni_bamberg.swl.tda.gui.model.Model;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;

/**
 * Controller for the Classes Requiring Attention tab.<br>
 * 
 * @author Christian Hild
 */

public class RequiringAttentionController extends AbstractFxmlPanelController
		implements XYChartController<String, Integer, TestedClass, Integer> {

	@FXML
	private BarChart<String, Integer> requiringAttentionChart;

	@FXML
	private Label classNameLabel;

	protected RequiringAttentionController(Model model) {

		super(EvolutionController.class.getResource("/de/uni_bamberg/swl/tda/gui/view/RequiringAttentionView.fxml"),
				I18N.getBundle(), model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void controllerDidLoadFxml() {
		requiringAttentionChart.setLegendVisible(false);
		clearChart();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onModelChange(Model model, Object args) {

		if (GuiConstants.LOADED_TESTRUNS_CHANGED.equals(args))
			populateChart();
	}

	private void populateChart() {
		clearChart();
		addSeries(I18N.getMessage("component.requiringAttentionView.seriesName"),
				getModel().getRequiringAttentionChartData());

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public XYChart<String, Integer> getChart() {
		return requiringAttentionChart;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String xToView(TestedClass testedClass) {
		int i = testedClass.getName().lastIndexOf('.');
		return testedClass.getName().substring(i + 1);
	}
}
