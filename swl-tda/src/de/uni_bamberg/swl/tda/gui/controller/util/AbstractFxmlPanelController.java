package de.uni_bamberg.swl.tda.gui.controller.util;

import de.uni_bamberg.swl.tda.gui.model.Model;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;

/**
 * Basic controller for panels loaded from FXML files.
 *
 * @author Florian Beetz
 */
public abstract class AbstractFxmlPanelController extends AbstractPanelController {

    private final URL fxmlURL;
    private final ResourceBundle resources;

    protected AbstractFxmlPanelController(URL fxmlURL, ResourceBundle resources, Model model) {
        super(model);
        this.fxmlURL = fxmlURL;
        this.resources = resources;
        assert fxmlURL != null : "Check the name of the FXML file used by "
                + getClass().getSimpleName();
    }

    @Override
    protected void makePanel() {
        final FXMLLoader loader = new FXMLLoader();

        loader.setController(this);
        loader.setLocation(fxmlURL);
        loader.setResources(resources);
        try {
            setPanelRoot(loader.load());
            controllerDidLoadFxml();
        } catch (RuntimeException | IOException x) {
            System.out.println("loader.getController()=" + loader.getController());
            System.out.println("loader.getLocation()=" + loader.getLocation());
            throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
        }
    }

    /**
     * Callback to notify implementors that the stage was created.
     */
    protected abstract void controllerDidLoadFxml();
}
