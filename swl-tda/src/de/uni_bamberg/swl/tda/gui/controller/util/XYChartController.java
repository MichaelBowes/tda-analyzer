package de.uni_bamberg.swl.tda.gui.controller.util;

import de.uni_bamberg.swl.tda.gui.DataPoint;

import java.util.List;

import javafx.scene.chart.XYChart;

/**
 * Controls a chart.
 *
 * @param <VIEW_X> the representation of x values.
 * @param <VIEW_Y> the representation of y values.
 * @param <X> type of x values.
 * @param <Y> type of y values.
 *
 * @author Florian Beetz
 */
public interface XYChartController<VIEW_X, VIEW_Y, X, Y> {

    /**
     * Returns the chart being controlled.
     * @return the chart.
     */
    XYChart<VIEW_X, VIEW_Y> getChart();

    /**
     * Clears the cart.
     */
    default void clearChart() {
        getChart().getData().clear();
    }

    /**
     * Adds a series to the chart.
     * @param name name of the series.
     * @param dataPoints points of the series.
     */
    default void addSeries(String name, List<DataPoint<X, Y>> dataPoints) {
        XYChart.Series<VIEW_X, VIEW_Y> series = new XYChart.Series<>();
        getChart().getData().add(series);
        controllerDidAddSeries(series, name);

        series.setName(name);

        for (DataPoint<X, Y> point : dataPoints) {
            XYChart.Data<VIEW_X, VIEW_Y> data = new XYChart.Data<>(
                    xToView(point.getX()),
                    yToView(point.getY())
            );

            series.getData().add(data);
            controllerDidAddPoint(point, data);
        }
    }

    /**
     * Callback to notify that a new series was added.
     * @param series the series.
     * @param name the name of the series.
     */
    default void controllerDidAddSeries(XYChart.Series<VIEW_X, VIEW_Y> series, String name) {
    }

    /**
     * Callback to notify that a new point was added.
     * @param point the point.
     * @param data the data.
     */
    default void controllerDidAddPoint(DataPoint<X, Y> point, XYChart.Data<VIEW_X, VIEW_Y> data) {
    }

    /**
     * Converts a x value to the value representing it in the chart.
     * @param x the value.
     * @return the representation.
     */
    @SuppressWarnings("unchecked")
    default VIEW_X xToView(X x) {
        return (VIEW_X) x;
    }

    /**
     * Converts a y value to the value representing it in the chart.
     * @param y the value.
     * @return the representation.
     */
    @SuppressWarnings("unchecked")
     default VIEW_Y yToView(Y y) {
         return (VIEW_Y) y;
     }
}
