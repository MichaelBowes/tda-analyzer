package de.uni_bamberg.swl.tda.gui;

import java.util.ResourceBundle;

/**
 * Provides functionality for internationalization.
 *
 * @author Florian Beetz
 */
public class I18N {

	private static ResourceBundle bundle;

	/**
	 * Returns the {@link ResourceBundle} used for TDA.
	 *
	 * @return the bundle.
	 */
	public static ResourceBundle getBundle() {
		if (bundle == null) {
			bundle = ResourceBundle.getBundle("de/uni_bamberg/swl/tda/gui/view/messages");
		}

		return bundle;
	}

	/**
	 * Returns the message with the given ID.
	 *
	 * @param id the id of the message.
	 *
	 * @return the message.
	 */
	public static String getMessage(String id) {
		return getBundle().getString(id);
	}

	/**
	 * Returns a parametrized message with the given ID.
	 * Uses {@code String.format(..)} internally.
	 *
	 * @param id the id of the message.
	 * @param args the parameters of the message.
	 *
	 * @return the parametrized message.
	 */
	public static String getMessage(String id, Object... args) {
		return String.format(getBundle().getString(id), args);
	}

	/**
	 * Short for {@link #getMessage(String)}
	 *
	 * @param id the id of the message.
	 *
	 * @return the message.
	 */
	public static String m(String id) {
		return getMessage(id);
	}

	/**
	 * Short for {@link #getMessage(String, Object...)}
	 *
	 * @param id the id of the message.
	 * @param args the parameters of the message.
	 *
	 * @return the parametrized message.
	 */
	public static String m(String id, Object... args) {
		return getMessage(id, args);
	}
}
