package de.uni_bamberg.swl.tda.gui;

import java.time.format.DateTimeFormatter;

/**
 * Contains constants shared across the GUI.
 */
public class GuiConstants {

	/** Default date time formatter for date times. */
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
			.ofPattern(I18N.m("data.datetime.format"));

	/**
	 * Signals that the the observable list
	 * {@link de.uni_bamberg.swl.tda.gui.model.ModelImpl#loadedTestRuns} was
	 * updated
	 */
	public static final String LOADED_TESTRUNS_CHANGED = "loadedTestRuns changed";
	/**
	 * Signals that the the observable list
	 * {@link de.uni_bamberg.swl.tda.gui.model.ModelImpl#selectedTestRuns} was
	 * updated
	 */
	public static final String SELECTED_TESTRUNS_CHANGED = "selectedTestRuns changed";

}
