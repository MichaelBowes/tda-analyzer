package de.uni_bamberg.swl.tda.gui.model;

import de.uni_bamberg.swl.tda.gui.DataPoint;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.apriori.AssociationRule;
import de.uni_bamberg.swl.tda.logic.apriori.ItemSet;
import de.uni_bamberg.swl.tda.logic.apriori.PackageDependency;
import javafx.beans.property.ListProperty;
import javafx.collections.ObservableList;
import javafx.scene.Scene;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Observable;

/**
 * Common base for the model.
 * <p>
 * The model should notify its listeners if it has changed. However, when values
 * that are observable by themselves the model should not notify its listeners.
 * <p>
 *
 * @author Florian Beetz
 * @author Michael Bowes
 * @author Christian Hild
 * @author Nicolas Bruch
 */
public abstract class Model extends Observable {

	/**
	 * Loads a list of XML files.
	 *
	 * @param files       the list of {@link File}s which should be loaded.
	 * @param parentScene the Scene of the parent window, which is locked while loading
	 */
	public abstract void loadXMLFiles(List<File> files, Scene parentScene);

	/**
	 * Returns the currently loaded {@link TestRun}s.
	 *
	 * @return the currently loaded {@link TestRun}s.
	 */
	public abstract ObservableList<TestRun> getLoadedTestRuns();

	/**
	 * Returns the unique {@link TestedClass}es of the loaded {@link TestRun}.
	 *
	 * @return the unique {@link TestedClass}es of the loaded {@link TestRun}.
	 */
	public abstract List<TestedClass> getUniqueTestedClasses();

	/**
	 * Returns the contents of the evolution chart.
	 * <p>
	 * The outer map contains the tested class to show the evolution off as key.
	 * The inner map contains the data points for a tested class over the
	 * evolution of the different test runs.
	 * <p>
	 *
	 * @return a map of maps of {@link TestRun}s that are currently loaded
	 */
	@Deprecated
	public abstract Map<String, Map<TestRun, Double>> getEvolutionChartData();

	/**
	 * Returns the contents of the evolution chart.
	 *
	 * @param classes the classes to get the data for.
	 *
	 * @return the data
	 */
	public abstract List<DataPoint<TestRun, Double>> getEvolutionChartData(List<TestedClass> classes);

	/**
	 * Notifies the model that the selected {@link TestedClass} has changed.
	 *
	 * @param selectedItems the selected items at the moment.
	 */
	public abstract void selectedClassChanged(ObservableList<TestedClass> selectedItems);

	/**
	 * Returns the selected classes in the
	 * {@link de.uni_bamberg.swl.tda.gui.controller.ClassSearchController}.
	 *
	 * <p>
	 * Only the name of the classes is guaranteed to be valid at any time.
	 *
	 * @return the selected classes as {@link ObservableList}.
	 */
	public abstract ObservableList<TestedClass> getSelectedTestedClasses();

	/**
	 * Returns the selected classes in the
	 * {@link de.uni_bamberg.swl.tda.gui.controller.ClassSearchController}.
	 *
	 * <p>
	 * Will return exactly the classes contained in the given {@link TestRun}.
	 * If the TestRun does not contain one or more selected classes, they will
	 * be left out from the list.
	 *
	 * @return the selected classes.
	 */
	public abstract List<TestedClass> getSelectedTestedClasses(TestRun run);

	/**
	 * Returns the property of the currently selected test run.
	 *
	 * @return the property of the currently selected test run.
	 */
	public abstract ListProperty<TestRun> selectedTestRunsProperty();

	/**
	 * Returns the currently selected {@link TestRun}.
	 *
	 * @return the currently selected {@link TestRun}.
	 * @see #selectedTestRunsProperty()
	 */
	public abstract List<TestRun> getSelectedTestRuns();

	public abstract void setSelectedTestRuns(List<TestRun> selected);

	/**
	 * Returns a List of observable lists. Every observable list contains two
	 * strings representing the name and the failure percentage of the
	 * corresponding TestedClass.
	 * 
	 * @return a List containing an Observable List representing a Table Row,
	 *         where index 0 is the class name and index 1 is the average
	 *         failure percentage.
	 */
	public abstract List<ObservableList<String>> getProblematicClassesData();

	/**
	 * Returns the minimal confidence for dependencies.
	 *
	 * @return the minimal confidence.
	 */
	public abstract double getDependencyMinConfidence();

	/**
	 * Returns the minimal support for dependencies.
	 *
	 * @return the minimal support.
	 */
	public abstract double getDependencyMinSupport();

	/**
	 * Sets the minmimal confidence for dependencies.
	 * 
	 * @param minConf
	 */
	public abstract void setDependencyMinConfidence(double minConf);

	/**
	 * Sets the minimal support for dependencies.
	 *
	 * @param minSup the minimal support.
	 */
	public abstract void setDependencyMinSupport(double minSup);

	/**
	 * Set the apriori parameters changed. Starts a new calculation.
	 */
	public abstract void aprioriParameterChanged();

	/**
	 * Save value into the user preferences.
	 */
	public abstract void saveSettings();

	/**
	 * Returns if the logger is enable or disable
	 *
	 * @return the status of the logger.
	 */
	public abstract boolean getLoggingStatus();

	/**
	 * Setter for logger. Enables or disables the logger.
	 *
	 * @param loggingStatus true actives the logger false disables the logger
	 */
	public abstract void setLoggingStatus(boolean loggingStatus);

	/**
	 * Returns the {@link AssociationRule}s for dependencies.
	 *
	 * @return a {@link List} of {@link AssociationRule}s.
	 */
	public abstract List<AssociationRule<TestedClass>> getDependencyRules();

	/**
	 * Returns the frequent {@link ItemSet}s.
	 *
	 * @return a {@link List} of the frequent {@link ItemSet}s.
	 */
	public abstract List<ItemSet<TestedClass>> getFrequentItemSets();

	/**
	 * Returns the dependent packages.
	 *
	 * @return a {@link List} of {@link PackageDependency}s.
	 */
	public abstract List<PackageDependency> getPackageDependencies();

	/**
	 * Returns the contents of the performance chart.
	 *
	 * @param classes the classes to get the data for.
	 *
	 * @return the data
	 */
	public abstract List<DataPoint<TestRun, Long>> getPerformanceChartData(List<TestedClass> classes);

	/**
	 * Resets the outcomes of the Apriori algorithm.
	 */
	public abstract void resetApriori();

	/**
	 * Returns the contents of the classes requiring attention chart.
	 *
	 * @return the data
	 */
	public abstract List<DataPoint<TestedClass, Integer>> getRequiringAttentionChartData();

}
