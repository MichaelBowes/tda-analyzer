package de.uni_bamberg.swl.tda.gui.model;

import de.uni_bamberg.swl.tda.gui.DataPoint;
import de.uni_bamberg.swl.tda.gui.GuiConstants;
import de.uni_bamberg.swl.tda.gui.GuiUtil;
import de.uni_bamberg.swl.tda.gui.Main;
import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.apriori.*;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

/**
 * Default implementation of the {@link Model}.
 *
 * @author Florian Beetz
 * @author Christian Hild
 * @author Michael Bowes
 * @author Nicolas Bruch
 * @author Nicolas Gross
 */

public class ModelImpl extends Model {

	private static final Logger LOGGER = Logger.getLogger(ModelImpl.class.getName());
	private static final String SETTINGS_MIN_SUPPORT = "MIN_SUPPORT";
	private static final String SETTINGS_MIN_CONFIDENCE = "MIN_CONFIDENCE";
	private static final String SETTINGS_LOGGER = "LOGGER";
	private static final double DEFAULT_MIN_SUPPORT_PERC = 0.6;
	private static final double DEFAULT_MIN_CONFIDENCE_PERC = 0.7;
	final ObservableList<TestRun> loadedTestRuns = FXCollections.observableArrayList();
	private final ListProperty<TestRun> selectedTestRuns = new SimpleListProperty<>(
			javafx.collections.FXCollections.observableList(new ArrayList<TestRun>()));
	private final Preferences prefs = Preferences.userNodeForPackage(this.getClass());
	ObservableList<TestedClass> selectedTestedClass = FXCollections.observableArrayList();
	private boolean loggingStatus;
	private double actualMinSupportPerc = 0.0;
	private double actualMinConfidencePerc = 0.0;
	private Apriori apriori = new Apriori(new ArrayList<>());

	/**
	 * Creates a new {@code ModelImpl}.
	 */
	public ModelImpl() {
		selectedTestRuns
				.addListener((InvalidationListener) observable -> setChanges(GuiConstants.SELECTED_TESTRUNS_CHANGED));
		loadedTestRuns
				.addListener((InvalidationListener) observable -> {
					try {
						apriori = new Apriori(loadedTestRuns, actualMinSupportPerc, actualMinConfidencePerc);
						apriori.analyze();
					} catch (TdaAprioriException e) {
						GuiUtil.exceptionAlert(e);
					}
					setChanges(GuiConstants.LOADED_TESTRUNS_CHANGED);
				});
		loadSettings();
	}

	/**
	 * Filters distinct values by a key.
	 *
	 * @param keyExtractor the function that extracts the key from the object.
	 * @param <T>          type of the object to extract the key from.
	 * @return a {@link Predicate} filtering distinct key values.
	 * @see <a href="http://stackoverflow.com/a/27872852">StackOverflow</a>
	 */
	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	/**
	 * This methods loads the user settings. It uses the preferences api. Use
	 * this method to load the values, which can be set in the settings dialog.
	 */
	private void loadSettings() {
		setDependencyMinSupport(prefs.getDouble(SETTINGS_MIN_SUPPORT, DEFAULT_MIN_SUPPORT_PERC));
		setDependencyMinConfidence(prefs.getDouble(SETTINGS_MIN_CONFIDENCE, DEFAULT_MIN_CONFIDENCE_PERC));
		setLoggingStatus(prefs.getBoolean(SETTINGS_LOGGER, true));

		if (!getLoggingStatus()) {
			LOGGER.info("logging is disabled - to reactivate logging please use settings dialog");
			LogManager.getLogManager().reset();
		}
		LOGGER.info("loaded user settings");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getLoggingStatus() {

		return loggingStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoggingStatus(boolean loggingStatus) {
		if (this.loggingStatus && !loggingStatus) {
			LOGGER.info("logging is disabled - to reactivate logging please use settings dialog");
			LogManager.getLogManager().reset();
		}
		if (!this.loggingStatus && loggingStatus) {
			try {
				LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("/logging.properties"));
			} catch (IOException e) {
				System.err.println("Failed to load logging properties, logging may not work as expected.");
				e.printStackTrace();
			}
			LOGGER.info("activated logging in settings dialog");
		}

		this.loggingStatus = loggingStatus;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveSettings() {
		prefs.putDouble(SETTINGS_MIN_SUPPORT, getDependencyMinSupport());
		prefs.putDouble(SETTINGS_MIN_CONFIDENCE, getDependencyMinConfidence());
		prefs.putBoolean(SETTINGS_LOGGER, loggingStatus);
		LOGGER.info("saved user settings");
		setChanges();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void loadXMLFiles(List<File> files, Scene parentScene) {
		LOGGER.info("Loading " + files.size() + " files");
		long start = System.currentTimeMillis();
		Task<List<TestRun>> parseTestRuns = new XMLLoader(files);
		Stage progressStage = GuiUtil.showProgressBar(parseTestRuns, parentScene);

		parseTestRuns.setOnSucceeded(event -> {
			List<TestRun> oldAndNewTestRuns = new LinkedList<>();
			while (true) {
				try {
					oldAndNewTestRuns.addAll(parseTestRuns.get().stream()
							.filter(testRun -> !loadedTestRuns.contains(testRun)).collect(Collectors.toList()));
					break;
				} catch (InterruptedException ignored) {
				} catch (ExecutionException e) {
					GuiUtil.exceptionAlert(e);
				}
			}
			oldAndNewTestRuns.addAll(loadedTestRuns);
			LOGGER.info("Loading took " + (System.currentTimeMillis() - start) + "ms");

			loadedTestRuns.setAll(oldAndNewTestRuns);
			loadedTestRuns.sort(null);
			progressStage.close();
		});

		parseTestRuns.setOnFailed(event -> {
			progressStage.close();
			while (true) {
				try {
					parseTestRuns.get();
					return;
				} catch (InterruptedException ignored) {
				} catch (ExecutionException e) {
					GuiUtil.exceptionAlert(e);
					return;
				}
			}
		});

		CompletableFuture.runAsync(parseTestRuns);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableList<TestRun> getLoadedTestRuns() {
		return loadedTestRuns;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TestedClass> getUniqueTestedClasses() {
		return selectedTestRuns.stream().flatMap(testRun -> testRun.getClassList().stream())
				.filter(distinctByKey(TestedClass::getName)).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Map<TestRun, Double>> getEvolutionChartData() {
		if (selectedTestedClass == null)
			return new LinkedHashMap<>();

		LinkedHashMap<TestRun, Double> internalData = new LinkedHashMap<>();
		LinkedHashMap<String, Map<TestRun, Double>> evoData = new LinkedHashMap<>();

		for (TestRun run : selectedTestRuns) {

			double failurePercentageSum = 0;
			int currentDuration = 0;
			for (TestedClass testedClass : selectedTestedClass) {
				failurePercentageSum += testedClass.getFailurePercentage();
				currentDuration++;

			}
			internalData.put(run, failurePercentageSum / currentDuration);
			if (selectedTestedClass.size() == 1)
				evoData.put(selectedTestedClass.get(0).getName(), internalData);
			else if (selectedTestedClass.size() == 0)
				evoData.put("Please select 2 or more TestRuns with CTRL and select a class", internalData);
			else
				evoData.put("Can not display evolution while more than 1 class is selected.", internalData);
		}
		return evoData;
	}
	
	@Override
	public List<DataPoint<TestRun, Double>> getEvolutionChartData(List<TestedClass> classes) {
		if (classes == null)
			return Collections.emptyList();

		List<DataPoint<TestRun, Double>> result = new ArrayList<>();
		for (TestRun run : selectedTestRuns) {
			double failurePercentageSum = 0;
			int testedClassesCount = 0;

			for (TestedClass testedClass : classes) {
				try {
					failurePercentageSum += run.getTestedClass(testedClass.getName()).getFailurePercentage();
					testedClassesCount++;
				} catch (TdaDataModelException e) {
					// class not found, can be ignored, count is not incremented
					// in that case
				}
			}
			double averageFailurePercentage = failurePercentageSum / testedClassesCount;

			result.add(new DataPoint<>(run, averageFailurePercentage));
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ObservableList<String>> getProblematicClassesData() {
		List<ObservableList<String>> classList = new ArrayList<>();
		// Go through every TestedClass in selectedTestRuns
		for (TestRun a : selectedTestRuns) {
			for (TestedClass b : a.getClassList()) {
				ObservableList<String> data = FXCollections.observableArrayList();
				data.add(b.getName());
				double failurepercentage = 0;
				for (TestRun c : selectedTestRuns) {
					for (TestedClass d : c.getClassList()) {
						if (b.getName().equals(d.getName())) {
							failurepercentage += d.getFailurePercentage();
						}
					}
				}
				failurepercentage = Math.round((failurepercentage / selectedTestRuns.size()) * 100.0) / 100.0;
				data.add(Double.toString(failurepercentage));
				// check if the TestedClass is already in the List.
				boolean contains = false;

				for (ObservableList<String> g : classList) {
					if (g.get(0).equals(data.get(0))) {
						contains = true;
					}
				}
				if (!contains) {
					classList.add(data);
				}
			}
		}
		return classList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void selectedClassChanged(ObservableList<TestedClass> selectedItems) {
		selectedTestedClass = selectedItems;
		setChanges();
	}

	@Override
	public ObservableList<TestedClass> getSelectedTestedClasses() {
		return selectedTestedClass;
	}

	@Override
	public List<TestedClass> getSelectedTestedClasses(TestRun run) {
		return selectedTestedClass.stream().map(identifier -> {
			try {
				return run.getTestedClass(identifier.getName());
			} catch (TdaDataModelException e) {
				// ignore
				return null;
			}
		}).filter(testedClass -> testedClass != null).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ListProperty<TestRun> selectedTestRunsProperty() {
		return selectedTestRuns;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TestRun> getSelectedTestRuns() {
		return selectedTestRuns;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSelectedTestRuns(List<TestRun> selected) {
		selectedTestRuns.clear();
		selectedTestRuns.addAll(selected);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getDependencyMinConfidence() {

		return this.actualMinConfidencePerc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDependencyMinConfidence(double minConf) {

		if (minConf >= 0 && minConf <= 1) {
			this.actualMinConfidencePerc = minConf;
		} else {
			throw new InvalidParameterException("Min Confidence must be a double between 0 and 1");
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getDependencyMinSupport() {
		return this.actualMinSupportPerc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDependencyMinSupport(double minSup) {

		if (minSup >= 0 && minSup <= 1) {
			this.actualMinSupportPerc = minSup;
		} else {
			throw new InvalidParameterException("Min Support must be a double between 0 and 1");

		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AssociationRule<TestedClass>> getDependencyRules() {
		return apriori.getStrongRules();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ItemSet<TestedClass>> getFrequentItemSets() {
		return apriori.getFrequentItemSets();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PackageDependency> getPackageDependencies() {
		return apriori.getPackageDependencies();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DataPoint<TestRun, Long>> getPerformanceChartData(List<TestedClass> classes) {
		if (classes == null)
			return Collections.emptyList();

		List<DataPoint<TestRun, Long>> result = new ArrayList<>();
		for (TestRun run : selectedTestRuns) {
			Duration duration = Duration.ZERO;
			for (TestedClass identifierClass : classes) {
				try {
					TestedClass testedClass = run.getTestedClass(identifierClass.getName());
					for (UnitTest test : testedClass.getTestList()) {
						duration = duration.plus(test.getResult().getDuration());
					}
				} catch (TdaDataModelException e) {
					// tested class is not contained in that run, duration is
					// zero, can be ignored
				}
			}

			result.add(new DataPoint<>(run, duration.toMillis()));
		}

		return result;
	}

	@Override
	public List<DataPoint<TestedClass, Integer>> getRequiringAttentionChartData() {

		if (loadedTestRuns == null || loadedTestRuns.isEmpty() || loadedTestRuns.size() == 1)
			return Collections.emptyList();

		List<DataPoint<TestedClass, Integer>> result = new ArrayList<>();

		List<TestedClass> testedClassesFromLastTestRun = loadedTestRuns.get(loadedTestRuns.size() - 1).getClassList();
		LOGGER.fine("Last testrun: " + loadedTestRuns.get(loadedTestRuns.size() - 1).getName());

		if (testedClassesFromLastTestRun.isEmpty()) {
			LOGGER.warning("Last test run has no tested classes.");
			return Collections.emptyList();
		}

		for (TestedClass currentTestedClass : testedClassesFromLastTestRun) {

			if (currentTestedClass.getFailurePercentage() > 0) {

				java.util.ListIterator<TestRun> testRunIterator = loadedTestRuns
						.listIterator(loadedTestRuns.size() - 1);

				LOGGER.fine("Found Class, which fails for the first time. " + currentTestedClass.getName());
				int testRunCounter = 1;
				boolean found = true;

				while (testRunIterator.hasPrevious() && found) {
					TestRun currentTestRun = testRunIterator.previous();
					try {
						if (currentTestRun.getClassList().contains(currentTestedClass) && currentTestRun
								.getTestedClass(currentTestedClass.getName()).getFailurePercentage() > 0) {
							testRunCounter++;
						} else {
							found = false;
						}
					} catch (TdaDataModelException e) {
						LOGGER.warning("Unable to check the failure percentage of " + currentTestedClass.getName());
					}
				}

				if (testRunCounter > 1) {
					result.add(new DataPoint<>(currentTestedClass, testRunCounter));

					LOGGER.fine("Found Class, which requires attention! " + currentTestedClass.getName()
							+ " failed since " + testRunCounter + " times.");
				}

			}
		}

		return result;
	}

	/**
	 * Notifies observers that an unspecified change happened.
	 */
	private void setChanges() {
		setChanged();
		notifyObservers();
		LOGGER.fine("Model changed.");
	}

	/**
	 * Notifies observers that a change happened.
	 *
	 * @param args argument to give to the observers.
	 */
	private void setChanges(Object args) {
		setChanged();
		notifyObservers(args);
		LOGGER.fine("Model changed: " + args);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void aprioriParameterChanged() {
		apriori.analyze();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resetApriori() {
		apriori = new Apriori(loadedTestRuns);
	}

}
