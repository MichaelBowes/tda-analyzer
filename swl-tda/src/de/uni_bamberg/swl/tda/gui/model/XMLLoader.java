package de.uni_bamberg.swl.tda.gui.model;

import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.parser.Parser;
import javafx.concurrent.Task;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * This class is used to load XML files in parallel.
 *
 * @author Nicolas Gross
 *
 */

class XMLLoader extends Task<List<TestRun>> {

	private static final Logger LOGGER = Logger.getLogger(XMLLoader.class.getName());

	private final List<File> files;

	public XMLLoader(List<File> files) {
		this.files = files;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TestRun> call() throws Exception {
		ExecutorService executor = Executors.newCachedThreadPool();
		List<Future<TestRun>> futureTestRuns = new LinkedList<>();
		try {
			for (File file : files) {
				LOGGER.info("Loading file: " + file);
				futureTestRuns.add(executor.submit(() -> Parser.parse(file)));
			}
			return waitForFutures(futureTestRuns);
		} finally {
			executor.shutdown();
			while (!executor.isTerminated()) {
				try {
					executor.awaitTermination(100, TimeUnit.MILLISECONDS);
				} catch (InterruptedException ignored) {
				}
			}
		}
	}

	private List<TestRun> waitForFutures(List<Future<TestRun>> futureTestRuns) throws ExecutionException {
		int i = 0;
		List<TestRun> testRuns = new LinkedList<>();
		for (Future<TestRun> futureTestRun : futureTestRuns) {
			while (true) {
				try {
					testRuns.add(futureTestRun.get());
					break;
				} catch (InterruptedException ignored) {
				}
			}
			updateProgress(++i, files.size());
		}
		return testRuns;
	}

}
