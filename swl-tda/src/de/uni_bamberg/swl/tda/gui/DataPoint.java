package de.uni_bamberg.swl.tda.gui;

/**
 * Data points of two correlating values.
 *
 * @param <X> type of the x value.
 * @param <Y> type of the y value.
 */
public class DataPoint<X, Y> {

    private final X x;
    private final Y y;

    public DataPoint(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public X getX() {
        return x;
    }

    public Y getY() {
        return y;
    }
}
