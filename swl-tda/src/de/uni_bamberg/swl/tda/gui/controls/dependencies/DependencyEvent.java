package de.uni_bamberg.swl.tda.gui.controls.dependencies;

import de.uni_bamberg.swl.tda.logic.apriori.AssociationRule;
import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 * Signals that an event occurred for a {@link DependencyMatrix}.
 *
 * @author Florian Beetz
 */
public class DependencyEvent<T> extends Event {

	/**
	 * Signals that a dependency was clicked.
	 */
	public static final EventType<DependencyEvent> CLICKED = new EventType<>("CLICKED");

	private final AssociationRule<T> rule;

	public DependencyEvent(@NamedArg("source") Object source,
						   @NamedArg("target") EventTarget target,
						   @NamedArg("eventType") EventType<? extends Event> eventType,
						   @NamedArg("rule") AssociationRule<T> rule) {
		super(source, target, eventType);
		this.rule = rule;
	}

	/**
	 * Returns the rule.
	 *
	 * @return
	 */
	public AssociationRule<T> getRule() {
		return rule;
	}
}
