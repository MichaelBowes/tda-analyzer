package de.uni_bamberg.swl.tda.gui.controls.dependencies;

import de.uni_bamberg.swl.tda.gui.I18N;
import de.uni_bamberg.swl.tda.logic.apriori.AssociationRule;
import de.uni_bamberg.swl.tda.logic.apriori.ItemSet;
import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * DependencyMatrix visualizes a set of {@link AssociationRule}s by drawing a
 * matrix of their preconditions and implications.
 * <p>
 * The rules are drawn as circles on the matrix. The size and color of the
 * circles represents their support and confidence.
 *
 * @author Florian Beetz
 */
public class DependencyMatrix<T> extends GridPane {

	/**
	 * Height of the labels.
	 */
	// FIXME: JavaFX screws up calculating dimensions of rotated nodes, remove
	// this when this is fixed
	private static final double LABEL_HEIGHT = 16;

	private static final Logger LOGGER = Logger.getLogger(DependencyMatrix.class.getName());

	private static final DecimalFormat decimalFormat = new DecimalFormat(I18N.m("data.percent.numberFormat"));

	@FXML
	private ScrollPane implicationsScrollPane;

	@FXML
	private HBox implicationsPane;

	@FXML
	private ScrollPane preconditionsScrollPane;

	@FXML
	private VBox preconditionsPane;

	@FXML
	private Pane matrixPane;

	@FXML
	private ScrollPane matrixScrollPane;

	@FXML
	private Label minSupportLabel;

	@FXML
	private Label minConfidenceLabel;

	private final ObservableList<AssociationRule<T>> associationRules = FXCollections.observableArrayList();
	private final Map<ItemSet<T>, Label> preconditionTexts = new HashMap<>();
	private final Map<ItemSet<T>, Group> implicationTexts = new HashMap<>();
	private final Map<ItemSet<T>, Double> xPositions = new HashMap<>();
	private final Map<ItemSet<T>, Double> yPositions = new HashMap<>();

	private final ObjectProperty<Function<T, String>> shortTextFactory = new SimpleObjectProperty<>(
			(value) -> value == null ? null : value.toString());
	private final ObjectProperty<Function<T, String>> longTextFactory = new SimpleObjectProperty<>(
			(value) -> value == null ? null : value.toString());
	private final ObjectProperty<Function<AssociationRule<T>, String>> ruleDescriptionFactory = new SimpleObjectProperty<>(
			(rule) -> I18N.m("data.association.format", rule.getSupport(), rule.getConfidence()));
	private final ObjectProperty<EventHandler<DependencyEvent>> eventHandler = new SimpleObjectProperty<>();
	private final ObjectProperty<Color> baseColor = new SimpleObjectProperty<>(Color.RED);
	private final ObjectProperty<Color> fadeColor = new SimpleObjectProperty<>(Color.LIGHTGRAY);
	private final DoubleProperty minSupport = new SimpleDoubleProperty(0);
	private final DoubleProperty minConfidence = new SimpleDoubleProperty(0);

	public DependencyMatrix() {
		loadFxml();
		setupDimensions();
		setupSynchronousScrolling();
		setupRedrawListener();
		setupLegendListener();
		redraw();
	}

	/*
	 * PUBLIC API
	 */

	/**
	 * Returns the {@link AssociationRule}s to display.
	 *
	 * @return rules to display.
	 */
	public ObservableList<AssociationRule<T>> getAssociationRules() {
		return associationRules;
	}

	/**
	 * Returns the base color of the {@link AssociationRule}s.
	 *
	 * @return base color.
	 */
	public Color getBaseColor() {
		return baseColor.get();
	}

	/**
	 * Returns the property of the base color of the {@link AssociationRule}s.
	 *
	 * @return property of the base color.
	 */
	public ObjectProperty<Color> baseColorProperty() {
		return baseColor;
	}

	/**
	 * Sets the base color of the {@link AssociationRule}s.
	 *
	 * @param baseColor
	 *            base color.
	 */
	public void setBaseColor(Color baseColor) {
		this.baseColor.set(baseColor);
	}

	/**
	 * Returns the color to fade the {@link AssociationRule}s out to.
	 *
	 * @return color to fade to.
	 */
	public Color getFadeColor() {
		return fadeColor.get();
	}

	/**
	 * Returns the property of the color to fade the {@link AssociationRule}s
	 * out to.
	 *
	 * @return property of fade color.
	 */
	public ObjectProperty<Color> fadeColorProperty() {
		return fadeColor;
	}

	/**
	 * Sets the color to fade the {@link AssociationRule}s out to.
	 *
	 * @param fadeColor
	 *            fade color.
	 */
	public void setFadeColor(Color fadeColor) {
		this.fadeColor.set(fadeColor);
	}

	/**
	 * Returns the {@link ItemSet} short description factory.
	 *
	 * @return factory for short descriptions.
	 */
	public Function<T, String> getShortTextFactory() {
		return shortTextFactory.get();
	}

	/**
	 * Returns the property of the {@link ItemSet} short description factory.
	 *
	 * @return property of factory for short descriptions.
	 */
	public ObjectProperty<Function<T, String>> shortTextFactoryProperty() {
		return shortTextFactory;
	}

	/**
	 * Sets the short description factory of {@link ItemSet}s.
	 *
	 * @param shortTextFactory
	 *            short description factory.
	 */
	public void setShortTextFactory(Function<T, String> shortTextFactory) {
		this.shortTextFactory.set(shortTextFactory);
	}

	/**
	 * Returns the {@link ItemSet} description factory.
	 *
	 * @return description factory.
	 */
	public Function<T, String> getLongTextFactory() {
		return longTextFactory.get();
	}

	/**
	 * Returns the property of the {@link ItemSet} description factory.
	 *
	 * @return description factory property.
	 */
	public ObjectProperty<Function<T, String>> longTextFactoryProperty() {
		return longTextFactory;
	}

	/**
	 * Sets the factory to generate long descriptive texts for the items of
	 * {@link ItemSet}s.
	 *
	 * @param longTextFactory
	 *            description factory.
	 */
	public void setLongTextFactory(Function<T, String> longTextFactory) {
		this.longTextFactory.set(longTextFactory);
	}

	/**
	 * Returns the {@link AssociationRule} factory.
	 *
	 * @return description factory.
	 */
	public Function<AssociationRule<T>, String> getRuleDescriptionFactory() {
		return ruleDescriptionFactory.get();
	}

	/**
	 * Returns the property of the {@link AssociationRule} description factory.
	 * 
	 * @return description factory property.
	 */
	public ObjectProperty<Function<AssociationRule<T>, String>> ruleDescriptionFactoryProperty() {
		return ruleDescriptionFactory;
	}

	/**
	 * Sets the factory to use to create descriptions of
	 * {@link AssociationRule}s.
	 *
	 * @param ruleDescriptionFactory
	 *            factory for descriptions.
	 */
	public void setRuleDescriptionFactory(Function<AssociationRule<T>, String> ruleDescriptionFactory) {
		this.ruleDescriptionFactory.set(ruleDescriptionFactory);
	}

	/**
	 * Returns the minimal confidence.
	 *
	 * @return minimal confidence.
	 */
	public double getMinConfidence() {
		return minConfidence.get();
	}

	/**
	 * Returns the property of the minimal confidence.
	 *
	 * @return minimal confidence property.
	 */
	public DoubleProperty minConfidenceProperty() {
		return minConfidence;
	}

	/**
	 * Sets the minimal confidence.
	 *
	 * @param minConfidence
	 *            confidence to set.
	 */
	public void setMinConfidence(double minConfidence) {
		this.minConfidence.set(minConfidence);
	}

	/**
	 * Returns the minimal support.
	 *
	 * @return minimal support.
	 */
	public double getMinSupport() {
		return minSupport.get();
	}

	/**
	 * Returns the property of the minimal support.
	 *
	 * @return minimal support property.
	 */
	public DoubleProperty minSupportProperty() {
		return minSupport;
	}

	/**
	 * Sets the minimal support.
	 *
	 * @param minSupport
	 *            support to set.
	 */
	public void setMinSupport(double minSupport) {
		this.minSupport.set(minSupport);
	}

	/**
	 * Returns the event handler.
	 *
	 * @return the event handler.
	 */
	public EventHandler<DependencyEvent> getEventHandler() {
		return eventHandler.get();
	}

	/**
	 * Returns the property of the event handler.
	 *
	 * @return event handler property.
	 */
	public ObjectProperty<EventHandler<DependencyEvent>> eventHandlerProperty() {
		return eventHandler;
	}

	/**
	 * Sets the event handler.
	 *
	 * @param eventHandler
	 *            handler to set.
	 */
	public void setEventHandler(EventHandler<DependencyEvent> eventHandler) {
		this.eventHandler.set(eventHandler);
	}

	/*
	 * SETUP METHODS
	 */

	/**
	 * Loads the fxml base.
	 */
	private void loadFxml() {
		final FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("dependencyMatrix.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		loader.setResources(I18N.getBundle());

		try {
			LOGGER.finer("Loading FXML of " + loader.getLocation());
			loader.load();
			LOGGER.finer("FXML loaded");
		} catch (IOException e) {
			throw new RuntimeException("Failed to setup dependency matrix.", e);
		}
	}

	/**
	 * Sets up the synchronization of the scroll bars of the legends and matrix.
	 */
	private void setupSynchronousScrolling() {
		preconditionsScrollPane.vvalueProperty().bindBidirectional(matrixScrollPane.vvalueProperty());
		implicationsScrollPane.hvalueProperty().bindBidirectional(matrixScrollPane.hvalueProperty());
	}

	/**
	 * Sets up all listeners that cause a redraw.
	 */
	private void setupRedrawListener() {
		InvalidationListener listener = observable -> redraw();
		associationRules.addListener(listener);
		baseColor.addListener(listener);
		shortTextFactory.addListener(listener);
		longTextFactory.addListener(listener);
		ruleDescriptionFactory.addListener(listener);
		minSupport.addListener(listener);
		minConfidence.addListener(listener);
		eventHandler.addListener(listener);
	}

	/**
	 * Sets up all listeners that cause a change of the legend.
	 */
	private void setupLegendListener() {
		minSupport.addListener((o, ov, nv) -> minSupportLabel.setText(decimalFormat.format(nv)));
		minConfidence.addListener((o, ov, nv) -> minConfidenceLabel.setText(decimalFormat.format(nv)));
	}

	/**
	 * Sets up the dimensions of the matrix.
	 */
	private void setupDimensions() {
		preconditionsPane.heightProperty()
				.addListener(observable -> matrixPane.setPrefHeight(preconditionsPane.getHeight()));
		implicationsPane.widthProperty()
				.addListener(observable -> matrixPane.setPrefWidth(implicationsPane.getWidth()));
	}

	/*
	 * REDRAWING
	 */

	/**
	 * Redraws everything.
	 */
	private void redraw() {
		LOGGER.finer("redrawing");
		long start = System.currentTimeMillis();

		// clear everything
		preconditionsPane.getChildren().clear();
		implicationsPane.getChildren().clear();
		matrixPane.getChildren().clear();
		invalidateCaches();

		// add labels
		addLabels();

		// recalculate sizes
		applyCss();
		layout();

		// draw matrix
		drawGrid();
		drawRules();

		LOGGER.finer("redraw complete, took " + (System.currentTimeMillis() - start) + "ms");
	}

	/**
	 * Draws all labels in the legends.
	 */
	private void addLabels() {
		double xPosition = LABEL_HEIGHT / 2;
		double yPosition = LABEL_HEIGHT / 2;
		for (AssociationRule<T> rule : associationRules) {
			String precondition = getItemSetAsString(rule.getPremise());
			if (!preconditionTexts.containsKey(rule.getPremise())) {
				Label preconditionLabel = new Label(precondition);
				preconditionLabel.setTooltip(getItemSetTooltip(rule.getPremise()));
				preconditionLabel.setMinSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
				preconditionLabel.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
				preconditionsPane.getChildren().add(preconditionLabel);
				preconditionTexts.put(rule.getPremise(), preconditionLabel);
				yPositions.put(rule.getPremise(), yPosition);

				yPosition += LABEL_HEIGHT;
			}

			String implication = getItemSetAsString(rule.getConclusion());
			if (!implicationTexts.containsKey(rule.getConclusion())) {
				Label implicationLabel = new Label(implication);
				implicationLabel.setTooltip(getItemSetTooltip(rule.getConclusion()));
				implicationLabel.setMinSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
				implicationLabel.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
				// wrap in Group, this fixes rendering of rotated nodes
				Group implicationGroup = new Group(implicationLabel);

				// rotate implication
				implicationLabel.setRotate(270d);

				implicationsPane.getChildren().add(implicationGroup);
				implicationTexts.put(rule.getConclusion(), implicationGroup);
				xPositions.put(rule.getConclusion(), xPosition);

				// noinspection SuspiciousNameCombination
				xPosition += LABEL_HEIGHT;
			}
		}

		// recalculate the sizes of children
		preconditionsScrollPane.applyCss();
		preconditionsScrollPane.layout();
		implicationsScrollPane.applyCss();
		implicationsScrollPane.layout();
		preconditionsPane.applyCss();
		preconditionsPane.layout();
		implicationsPane.applyCss();
		implicationsPane.layout();

		// scroll to the max
		preconditionsScrollPane.setHvalue(preconditionsScrollPane.getHmax());
		implicationsScrollPane.setVvalue(implicationsScrollPane.getVmax());

		LOGGER.finer("Added labels for " + associationRules.size() + " rules.");
	}

	/**
	 * Draws the grid in the background of the matrix.
	 */
	private void drawGrid() {
		for (Map.Entry<ItemSet<T>, Label> entry : preconditionTexts.entrySet()) {
			double yPosition = yPositions.get(entry.getKey());
			Line line = getGridLine(0, yPosition, matrixPane.getWidth(), yPosition);
			matrixPane.getChildren().add(line);
		}

		for (Map.Entry<ItemSet<T>, Group> entry : implicationTexts.entrySet()) {
			double xPosition = xPositions.get(entry.getKey());
			Line line = getGridLine(xPosition, 0, xPosition, matrixPane.getHeight());
			matrixPane.getChildren().add(line);
		}
	}

	/**
	 * Draws all rules as circles.
	 */
	private void drawRules() {
		for (AssociationRule<T> rule : associationRules) {
			Circle circle = new Circle();
			circle.setFill(getPaintOfRule(rule));
			circle.setRadius(getRadiusOfRule(rule));

			circle.setCenterX(xPositions.get(rule.getConclusion()));
			circle.setCenterY(yPositions.get(rule.getPremise()));

			circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					fireEvent(new DependencyEvent<>(this, circle, DependencyEvent.CLICKED, rule));
				}
			});

			Tooltip.install(circle, getAssociationRuleTooltip(rule));

			matrixPane.getChildren().add(circle);
		}
	}

	/*
	 * UTIL
	 */

	/**
	 * Returns a line for the grid with the given start and end coordinates.
	 *
	 * @param x1
	 *            the x value of the start coordinate.
	 * @param y1
	 *            the y value of the start coordinate.
	 * @param x2
	 *            the x value of the end coordinate.
	 * @param y2
	 *            the y value of the end coordinate.
	 * @return the line.
	 */
	private Line getGridLine(double x1, double y1, double x2, double y2) {
		Line line = new Line(x1, y1, x2, y2);
		line.setStroke(Color.LIGHTGRAY);
		line.getStrokeDashArray().addAll(5d);
		return line;
	}

	/**
	 * Returns the {@link ItemSet} as a short descriptive string.
	 * <p>
	 * The string is created using {@link #shortTextFactory}.
	 *
	 * @param itemSet
	 *            the item set to get the string for.
	 * @return the item set as string.
	 */
	private String getItemSetAsString(ItemSet<T> itemSet) {
		return itemSet.toJava().stream().map(getShortTextFactory())
				.collect(Collectors.joining(I18N.m("data.sets.delimiter"), I18N.m("data.sets.prefix"), I18N.m("data.sets.suffix")));
	}

	/**
	 * Returns the tooltip to install on the labels of {@link ItemSet}s.
	 * <p>
	 * The tooltip is created using {@link #longTextFactory}.
	 *
	 * @param itemSet
	 *            the item set to get the tooltip of.
	 * @return the tooltip.
	 */
	private Tooltip getItemSetTooltip(ItemSet<T> itemSet) {
		return new Tooltip(
				itemSet.toJava().stream().map(getLongTextFactory())
						.collect(Collectors.joining(I18N.m("data.sets.delimiter"), I18N.m("data.sets.prefix"), I18N.m("data.sets.suffix"))));
	}

	/**
	 * Returns the tooltip to install on the circles representing
	 * {@link AssociationRule}s.
	 * <p>
	 * The tooltip is created using {@link #ruleDescriptionFactory}.
	 *
	 * @param rule
	 *            the rule to get the tooltip of.
	 * @return the tooltip.
	 */
	private Tooltip getAssociationRuleTooltip(AssociationRule<T> rule) {
		return new Tooltip(getRuleDescriptionFactory().apply(rule));
	}

	/**
	 * Clears all caches.
	 */
	private void invalidateCaches() {
		xPositions.clear();
		yPositions.clear();
		preconditionTexts.clear();
		implicationTexts.clear();
	}

	/**
	 * Calculates the color of a given {@link AssociationRule}.
	 * <p>
	 * The color is chosen based upon the confidence of the rule. It lies in
	 * between the color space of {@link #fadeColor} and {@link #baseColor}.
	 * <p>
	 * A rule with a confidence equal to {@link #minConfidence} will be painted
	 * in {@link #fadeColor}, a rule with a confidence equal to {@code 1} will
	 * be painted in {@link #baseColor}.
	 *
	 * @param rule
	 *            the rule to calculate the paint of.
	 * @return the calculated color.
	 */
	private Paint getPaintOfRule(AssociationRule<T> rule) {
		// adjust confidence of range minConfidence..1 to 0..1 as t for
		// interpolation
		double t = 1 / (1 - minConfidence.get()) * (rule.getConfidence() - minConfidence.get());
		return getFadeColor().interpolate(getBaseColor(), t);
	}

	/**
	 * Calculates the radius of a given {@link AssociationRule}.
	 * <p>
	 * The radius is chosen based upon the support of the rule, but will always
	 * be greater or equal {@code 3}.
	 *
	 * @param rule
	 *            the rule to calculate the radius of.
	 * @return the calculated radius.
	 */
	private double getRadiusOfRule(AssociationRule<T> rule) {
		// adjust support of range minSupport..1 to 3..(LABEL_HEIGHT/2) as
		// radius
		return (LABEL_HEIGHT / 2 - 3) * (rule.getSupport() - getMinSupport()) / (1 - getMinSupport()) + 3;
	}

	/**
	 * Fires a {@link DependencyEvent} to the handler.
	 *
	 * @param event
	 *            the event to fire.
	 */
	private void fireEvent(DependencyEvent event) {
		if (getEventHandler() != null) {
			getEventHandler().handle(event);
		}
	}
}
