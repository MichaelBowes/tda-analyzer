package de.uni_bamberg.swl.tda.logic.parser;

import java.util.Optional;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

/**
 * Provides some utility methods for dealing with XML.
 *
 * @author Michael Bowes
 */
class XMLUtilities {

    /**
     * Returns an {@link Optional}&lt;String&gt; of the value of an attribute on an element.
     * @param element the element to get the attribute from.
     * @param name the name of the attribute.
     * @return the value of the attribute. If the attribute is not present on the element
     *          an empty {@link Optional} is returned.
     */
    protected Optional<String> getAttribute(StartElement element, String name) {
        Attribute attribute = element.getAttributeByName(new QName(name));

        return Optional.ofNullable(attribute).map(Attribute::getValue);
    }

    /**
     * Returns the value of an attribute on an element or throws a {@link ParserException} with the
     * given message.
     * @param element the element to get the attribute from.
     * @param name the name of the attribute.
     * @param message the message of the exception to throw.
     *
     * @return the value of the attribute.
     *
     * @throws ParserException if the attribute is not present.
     */
    protected String getAttributeOrThrow(StartElement element, String name, String message)
            throws ParserException {
        return getAttribute(element, name).orElseThrow(() -> new ParserException(message));
    }


    /**
     * Returns the value of an attribute on an element or throws a {@link ParserException} with an
     * error message.
     *
     * @param element the element to get the attribute from.
     * @param name the name of the attribute.
     *
     * @return the value of the attribute.
     *
     * @throws ParserException if the attribute is not present.
     */
    protected String getAttributeOrThrow(StartElement element, String name) throws ParserException {
        String message = String.format(
                "Expected attribute '%s' on element '%s' is not present in XML.",
                name,
                element.getName().getLocalPart()
        );

        return getAttributeOrThrow(element, name, message);
    }

}
