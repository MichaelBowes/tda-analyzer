package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.UnitTestBuilder;

import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Parses the {@code <TestDefinitions>} element in the XML.
 * 
 * @author Michael Bowes
 */
public class TestDefinitionsContext extends ParsingContextBase {

	private final ParsingContext parent;
	
    private final Map<String, List<UnitTestBuilder>> unitTestsByClasses;
    private final Map<String, UnitTestBuilder> unitTestByExecutionId;

	private UnitTestBuilder unitTestBuilder;

    public TestDefinitionsContext(ParsingContext parent, Map<String, List<UnitTestBuilder>> unitTestsByClasses, Map<String, UnitTestBuilder> unitTestByExecutionId) {
    	this.parent = parent;
		this.unitTestsByClasses = unitTestsByClasses;
        this.unitTestByExecutionId = unitTestByExecutionId;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected ParsingContext handleStartElement(StartElement startElement) throws ParserException {
        // <UnitTest>
        if (XMLName.Element.UNIT_TEST.equals(startElement.getName().getLocalPart())) {
            unitTestBuilder = new UnitTestBuilder();
            unitTestBuilder.setId(getAttributeOrThrow(startElement, XMLName.Attribute.ID));
            unitTestBuilder.setName(getAttributeOrThrow(startElement, XMLName.Attribute.NAME));
            unitTestBuilder.setStorage(getAttributeOrThrow(startElement, XMLName.Attribute.STORAGE));
        }

        // <TestMethod>
        if (XMLName.Element.TEST_METHOD.equals(startElement.getName().getLocalPart())) {
        	// Local variable className is saved as String for further use in the unitTestBuilders List.
        	String className = getAttributeOrThrow(startElement, XMLName.Attribute.CLASS_NAME);
        	
            unitTestBuilder.setAdapterTypeName(getAttributeOrThrow(startElement, XMLName.Attribute.ADAPTER_TYPE_NAME));
            unitTestBuilder.setCodeBase(getAttributeOrThrow(startElement, XMLName.Attribute.CODE_BASE));
            
            // TODO: Small Explanation.
            List<UnitTestBuilder> unitTestBuilders = unitTestsByClasses.get(className);
            if (unitTestBuilders == null) {
            	unitTestBuilders = new ArrayList<>();
            }
            
            unitTestBuilders.add(unitTestBuilder);
            unitTestsByClasses.put(className, unitTestBuilders);
        }
        
        // <Execution>
        if (XMLName.Element.EXECUTION.equals(startElement.getName().getLocalPart())) {
        	String executionId = getAttributeOrThrow(startElement, XMLName.Attribute.ID);
        	unitTestByExecutionId.put(executionId, unitTestBuilder);
        }       

        return this;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected ParsingContext handleEndElement(EndElement endElement) throws ParserException {
        if (XMLName.Element.TEST_DEFINITIONS.equals(endElement.getName().getLocalPart())) {
            // switch back to parent context
            return parent;
        }

        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TestRun getResult() throws IllegalStateException, TdaDataModelException {
        throw new IllegalStateException(
                String.format("The result of the parsing operation could not be determined. " +
                        "The currently parsed element is '%s'.",
                        XMLName.Element.TEST_DEFINITIONS)
        );
    }
}
