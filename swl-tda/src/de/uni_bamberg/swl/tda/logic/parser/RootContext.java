package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestRunBuilder;
import de.uni_bamberg.swl.tda.logic.TestedClass;
import de.uni_bamberg.swl.tda.logic.UnitTest;
import de.uni_bamberg.swl.tda.logic.UnitTestBuilder;
import java.util.*;
import java.util.Map.Entry;

import javax.xml.stream.events.StartElement;

/**
 * Handles the parsing of the root element {@code <TestRun>} and its contained simple elements.
 *
 * @author Michael Bowes
 */
class RootContext extends ParsingContextBase {

    private final TestRunBuilder rootBuilder = new TestRunBuilder();

    private final Map<String, List<UnitTestBuilder>> unitTestsByClasses = new HashMap<>();
    private final Map<String, UnitTestBuilder> unitTestByExecutionId = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    protected ParsingContext handleStartElement(StartElement startElement) throws ParserException {

        // <TestRun>
        if (XMLName.Element.TEST_RUN.equals(startElement.getName().getLocalPart())) {
        	rootBuilder.setXmlns(startElement.getNamespaceURI(""));
        	rootBuilder.setId(getAttributeOrThrow(startElement, XMLName.Attribute.ID));
        	rootBuilder.setName(getAttributeOrThrow(startElement, XMLName.Attribute.NAME));
        	rootBuilder.setRunUser(getAttributeOrThrow(startElement, XMLName.Attribute.RUN_USER));
            return this;
        }

        // <TestSettings>
        if (XMLName.Element.TEST_SETTINGS.equals(startElement.getName().getLocalPart())) {
            TestSettingContext context = new TestSettingContext(this, rootBuilder);
            context.handleStartElement(startElement);
            return context;
        }

        // <Times>
        if (XMLName.Element.TIMES.equals(startElement.getName().getLocalPart())) {
            rootBuilder.setCreationDate(getAttributeOrThrow(startElement, XMLName.Attribute.CREATION));
            rootBuilder.setFinishDate(getAttributeOrThrow(startElement, XMLName.Attribute.FINISH));
            rootBuilder.setQueuingDate(getAttributeOrThrow(startElement, XMLName.Attribute.QUEUEING));
            rootBuilder.setStartDate(getAttributeOrThrow(startElement, XMLName.Attribute.START));
            return this;
        }

        // <ResultSummary>
        if (XMLName.Element.RESULT_SUMMARY.equals(startElement.getName().getLocalPart())) {
            ResultSummaryContext context = new ResultSummaryContext(this, rootBuilder);
            context.handleStartElement(startElement);
            return context;
        }

        // <TestDefinitions>
        if (XMLName.Element.TEST_DEFINITIONS.equals(startElement.getName().getLocalPart())) {
            TestDefinitionsContext context = new TestDefinitionsContext(this,
					unitTestsByClasses, unitTestByExecutionId);
            context.handleStartElement(startElement);
            return context;
        }

        // <TestLists>
        if (XMLName.Element.TEST_LISTS.equals(startElement.getName().getLocalPart())) {
            TestListsContext context = new TestListsContext(this);
            context.handleStartElement(startElement);
            return context;
        }

        // <TestEntries>
        if (XMLName.Element.TEST_ENTRIES.equals(startElement.getName().getLocalPart())) {
            TestEntriesContext context = new TestEntriesContext(this);
            context.handleStartElement(startElement);
            return context;
        }

        // <Results>
        if (XMLName.Element.RESULTS.equals(startElement.getName().getLocalPart())) {
            ResultsContext context = new ResultsContext(this, unitTestByExecutionId);
            context.handleStartElement(startElement);
            return context;
        }

        // unknown element
        throw new ParserException(String.format("Encountered an unknown element '%s' in XML.",
                startElement.getName().getLocalPart()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TestRun getResult() throws IllegalStateException, TdaDataModelException {

    	List<TestedClass> testedClasses = new ArrayList<>();
    	
    	for(Entry <String, List<UnitTestBuilder>> entry : unitTestsByClasses.entrySet()) {
    		ArrayList<UnitTest> tests = new ArrayList<>();
    		for (UnitTestBuilder unitTestBuilder : entry.getValue()) {
    			tests.add(unitTestBuilder.create());
    		}
    		
    		testedClasses.add(new TestedClass(entry.getKey(), tests));
    	}
    	
    	rootBuilder.setClassList(testedClasses);    	
    	
        return rootBuilder.create();
    }
}
