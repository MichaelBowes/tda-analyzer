package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * Provides parsing functionality to the application.
 *
 * @author Michael Bowes
 */
public class Parser {

    private static final Logger LOGGER = Logger.getLogger(Parser.class.getName());
    private static final XMLInputFactory FACTORY = XMLInputFactory.newFactory();

    /**
     * Parses XML from a {@link File} to a {@link TestRun}.
     *
     * @param file the file to parse.
     * @return the parsed {@link TestRun}.
     * @throws FileNotFoundException Signals that the given file could not be found.
     * @throws ParserException       Signals that an error occurred while parsing.
     * @see #parse(InputStream)
     */
    public static TestRun parse(File file) throws FileNotFoundException, ParserException {
        return parse(new FileInputStream(file));
    }

    /**
     * Parses XML form an {@link InputStream} to a {@link TestRun}.
     *
     * @param stream the input stream.
     * @return the parsed {@link TestRun}.
     * @throws ParserException Signals that an error occurred while parsing.
     */
    public static TestRun parse(InputStream stream) throws ParserException {
        try {
            LOGGER.fine("Starting parsing of XML stream");
            long start = System.currentTimeMillis();
            XMLEventReader reader = FACTORY.createXMLEventReader(stream);

            ParsingContext context = new RootContext();

            while (reader.hasNext()) {
                context = context.handleEvent(reader.nextEvent());
            }

            LOGGER.fine("Parsing took " + (System.currentTimeMillis() - start) + "ms");
            return context.getResult();
        } catch (XMLStreamException e) {
            throw new ParserException("Failed to read XML.", e);
        } catch (TdaDataModelException e) {
            throw new ParserException("Failed to parse XML. The structure was valid but the " +
                    "contents could not be validated.", e);
        }
    }

}
