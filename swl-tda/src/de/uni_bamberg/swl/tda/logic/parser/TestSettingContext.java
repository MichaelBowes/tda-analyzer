package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestRunBuilder;
import de.uni_bamberg.swl.tda.logic.TestRunSettingBuilder;

import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;

/**
 * Parses the {@code <TestSettings>} element in the XML.
 *
 * @author Michael Bowes
 */
class TestSettingContext extends ParsingContextBase {

    private final ParsingContext parent;
    private final TestRunBuilder rootBuilder;

    private final TestRunSettingBuilder testRunSettingsBuilder = new TestRunSettingBuilder();

    public TestSettingContext(ParsingContext parent, TestRunBuilder builder) {
        this.parent = parent;
        this.rootBuilder = builder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ParsingContext handleStartElement(StartElement startElement) throws ParserException {
        // <TestSettings>
        if (XMLName.Element.TEST_SETTINGS.equals(startElement.getName().getLocalPart())) {
            testRunSettingsBuilder.setId(getAttributeOrThrow(startElement, XMLName.Attribute.ID));
            testRunSettingsBuilder.setName(getAttributeOrThrow(startElement, XMLName.Attribute.NAME));
        }

        // <Deployment>
        if (XMLName.Element.DEPLOYMENT.equals(startElement.getName().getLocalPart())) {
            testRunSettingsBuilder.setRunDeploymentRoot(getAttributeOrThrow(startElement, XMLName.Attribute.RUN_DEPLOYMENT_ROOT));
            String useDeploymentRoot = getAttributeOrThrow(startElement, XMLName.Attribute.USER_DEPLOYMENT_ROOT);
            testRunSettingsBuilder.setUseDefaultDeploymentRoot(Boolean.parseBoolean(useDeploymentRoot));
            testRunSettingsBuilder.setUserDeploymentRoot(getAttributeOrThrow(startElement, XMLName.Attribute.USER_DEPLOYMENT_ROOT));
        }

        // <AgentRule>
        if (XMLName.Element.AGENT_RULE.equals(startElement.getName().getLocalPart())) {
            testRunSettingsBuilder.setAgentRuleName(getAttributeOrThrow(startElement, XMLName.Attribute.NAME));
        }

        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ParsingContext handleEndElement(EndElement endElement) throws ParserException {
        if (XMLName.Element.TEST_SETTINGS.equals(endElement.getName().getLocalPart())) {
            // build the settings
            try {
                rootBuilder.setSetting(testRunSettingsBuilder.create());
            } catch (TdaDataModelException e) {
                throw new ParserException(
                        String.format("Failed to parse '%s' element. " +
                                "The structure was valid, but the contents could not be validated.",
                                XMLName.Element.TEST_SETTINGS),
                        e
                );
            }

            // switch back to parent context
            return parent;
        }

        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TestRun getResult() throws IllegalStateException, TdaDataModelException {
        throw new IllegalStateException(
                String.format("The result of the parsing operation could not be determined. " +
                        "The currently parsed element is '%s'.",
                        XMLName.Element.TEST_SETTINGS)
        );
    }
}
