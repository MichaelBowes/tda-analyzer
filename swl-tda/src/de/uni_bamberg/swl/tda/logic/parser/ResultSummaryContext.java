package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.TdaDataModelException;
import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestRunBuilder;
import de.uni_bamberg.swl.tda.logic.TestRunResultBuilder;
import de.uni_bamberg.swl.tda.logic.Outcome;

import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;

/**
 * Parses the {@code <ResultSummary>} element in the XML.
 * 
 * @author Michael Bowes
 */
public class ResultSummaryContext extends ParsingContextBase {

	private final ParsingContext parent;
	private final TestRunBuilder rootBuilder;

	private boolean isCurrentlyInStdOut = false;

	private final TestRunResultBuilder testRunResultBuilder = new TestRunResultBuilder();

	public ResultSummaryContext(ParsingContext parent, TestRunBuilder builder) {
		this.parent = parent;
		this.rootBuilder = builder;
	}

	/**
	 * {@inheritDoc}
     */
	@Override
	protected ParsingContext handleStartElement(StartElement startElement) throws ParserException {

		// Initialize with empty String, null is not allowed
		testRunResultBuilder.setStdOut("");
		testRunResultBuilder.setErrorInfo("");

		// <ResultSummary>
		if (XMLName.Element.RESULT_SUMMARY.equals(startElement.getName().getLocalPart())) {
			// Local variable stringOutcome saves Outcome as String.
			// stringOutcome is saved as ENUM in outcome with the in-built toUpperCase() function for Strings. 
			String stringOutcome = getAttributeOrThrow(startElement, XMLName.Attribute.OUTCOME);
			Outcome outcome = Outcome.valueOf(stringOutcome.toUpperCase());
			testRunResultBuilder.setOutcome(outcome);
		}

		// <Counters>
		if (XMLName.Element.COUNTERS.equals(startElement.getName().getLocalPart())) {
			testRunResultBuilder.setAborted(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.ABORTED)));
			testRunResultBuilder.setCompleted(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.COMPLETED)));
			testRunResultBuilder.setDisconnected(
					Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.DISCONNECTED)));
			testRunResultBuilder.setError(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.ERROR)));
			testRunResultBuilder.setExecuted(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.EXECUTED)));
			testRunResultBuilder.setFailed(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.FAILED)));
			testRunResultBuilder.setInProgress(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.IN_PROGRESS)));
			testRunResultBuilder.setInconclusive(
					Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.INCONCLUSIVE)));
			testRunResultBuilder.setNotExecuted(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.NOT_EXECUTED)));
			testRunResultBuilder.setNotRunnable(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.NOT_RUNNABLE)));
			testRunResultBuilder.setPassed(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.PASSED)));
			testRunResultBuilder.setPassedButRunAborted(
					Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.PASSED_BUT_RUN_ABORTED)));
			testRunResultBuilder.setPending(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.PENDING)));
			testRunResultBuilder.setTimeout(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.TIMEOUT)));
			testRunResultBuilder.setTotal(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.TOTAL)));
			testRunResultBuilder.setWarning(Integer.parseInt(getAttributeOrThrow(startElement, XMLName.Attribute.WARNING)));
		}

		// <StdOut>
		if (XMLName.Element.STD_OUT.equals(startElement.getName().getLocalPart())) {
			isCurrentlyInStdOut = true;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ParsingContext handleCharacters(Characters characters) throws ParserException {

		// <Std_Out> within Output
		if (isCurrentlyInStdOut) {
			testRunResultBuilder.setStdOut(characters.getData());
			isCurrentlyInStdOut = false;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ParsingContext handleEndElement(EndElement endElement) throws ParserException {
		if (XMLName.Element.RESULT_SUMMARY.equals(endElement.getName().getLocalPart())) {
			// build summary of the results
			try {
				rootBuilder.setResult(testRunResultBuilder.create());
			} catch (TdaDataModelException e) {
				throw new ParserException(String.format(
						"Failed to parse '%s' element. "
								+ "The structure was valid, but the contents could not be validated.",
						XMLName.Element.RESULT_SUMMARY), e);
			}

			// switch back to parent context
			return parent;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
     */
	@Override
	public TestRun getResult() throws IllegalStateException, TdaDataModelException {
		throw new IllegalStateException(String.format("The result of the parsing operation could not be determined. "
				+ "The currently parsed element is '%s'.", XMLName.Element.RESULT_SUMMARY));
	}
}
