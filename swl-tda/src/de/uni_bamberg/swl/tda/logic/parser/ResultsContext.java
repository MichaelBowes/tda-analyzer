package de.uni_bamberg.swl.tda.logic.parser;

import de.uni_bamberg.swl.tda.logic.*;

import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.util.Map;

/**
 * Parses the {@code <Results>} element in the XML.
 * 
 * @author Michael Bowes
 */
public class ResultsContext extends ParsingContextBase {

	private final ParsingContext parent;
	private final Map<String, UnitTestBuilder> unitTestsByExecutionId;
	private final UnitTestResultBuilder unitTestResultBuilder = new UnitTestResultBuilder();

	private String currentExecutionId;
	private boolean isCurrentlyInStdOut = false;
	private boolean isCurrentlyInErrorInfo = false;

	public ResultsContext(ParsingContext parent, Map<String, UnitTestBuilder> unitTestsByExecutionId) {
		this.parent = parent;
		this.unitTestsByExecutionId = unitTestsByExecutionId;
	}

	/**
	 * {@inheritDoc}
     */
	@Override
	protected ParsingContext handleStartElement(StartElement startElement) throws ParserException {
		// <UnitTestResult>
		if (XMLName.Element.UNIT_TEST_RESULT.equals(startElement.getName().getLocalPart())) {
			
			// initialize with empty string, null is not allowed
			unitTestResultBuilder.setStdOut("");
			unitTestResultBuilder.setErrorInfo("");
			
			currentExecutionId = getAttributeOrThrow(startElement, XMLName.Attribute.EXECUTION_ID);

			String stringOutcome = getAttributeOrThrow(startElement, XMLName.Attribute.OUTCOME);
			Outcome outcome = Outcome.valueOf(stringOutcome.toUpperCase());
			unitTestResultBuilder.setOutcome(outcome);

			unitTestResultBuilder.setComputerName(getAttributeOrThrow(startElement, XMLName.Attribute.COMPUTER_NAME));
			unitTestResultBuilder.setDataRowInfo(getAttributeOrThrow(startElement, XMLName.Attribute.DATA_ROW_INFO));
			unitTestResultBuilder.setDuration(getAttributeOrThrow(startElement, XMLName.Attribute.DURATION));
			unitTestResultBuilder.setStartTime(getAttributeOrThrow(startElement, XMLName.Attribute.START_TIME));
			unitTestResultBuilder.setEndTime(getAttributeOrThrow(startElement, XMLName.Attribute.END_TIME));
			unitTestResultBuilder.setExecutionId(currentExecutionId);
			unitTestResultBuilder.setRelativeResultsDirectory(
					getAttributeOrThrow(startElement, XMLName.Attribute.RELATIVE_RESULTS_DIRECTORY));
			unitTestResultBuilder.setStartTime(getAttributeOrThrow(startElement, XMLName.Attribute.START_TIME));
			unitTestResultBuilder.setTestListId(getAttributeOrThrow(startElement, XMLName.Attribute.TEST_LIST_ID));
			unitTestResultBuilder.setTestType(getAttributeOrThrow(startElement, XMLName.Attribute.TEST_TYPE));
		}

		// <StdOut>
		if (XMLName.Element.STD_OUT.equals(startElement.getName().getLocalPart())) {
			isCurrentlyInStdOut = true;
		}

		// <ErrorInfo>
		if (XMLName.Element.ERROR_INFO.equals(startElement.getName().getLocalPart())) {
			isCurrentlyInErrorInfo = true;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ParsingContext handleCharacters(Characters characters) throws ParserException {

		// <Message> or <StdOut> within Output
		if (isCurrentlyInStdOut) {
			unitTestResultBuilder.setStdOut(characters.getData());
			isCurrentlyInStdOut = false;
		}

		if (isCurrentlyInErrorInfo) {
			unitTestResultBuilder.setErrorInfo(characters.getData());
			isCurrentlyInErrorInfo = false;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ParsingContext handleEndElement(EndElement endElement) throws ParserException {
		if (XMLName.Element.UNIT_TEST_RESULT.equals(endElement.getName().getLocalPart())) {
			try {
				UnitTestResult result = unitTestResultBuilder.create();

				UnitTestBuilder unitTestBuilder = unitTestsByExecutionId.get(currentExecutionId);
				unitTestBuilder.setResult(result);

			} catch (TdaDataModelException e) {
				throw new ParserException(String.format(
						"Failed to parse '%s' element. "
								+ "The structure was valid, but the contents could not be validated.",
						XMLName.Element.UNIT_TEST_RESULT), e);
			}
		}

		if (XMLName.Element.RESULTS.equals(endElement.getName().getLocalPart())) {
			// switch back to parent context
			return parent;
		}

		return this;
	}

	/**
	 * {@inheritDoc}
     */
	@Override
	public TestRun getResult() throws IllegalStateException, TdaDataModelException {
		throw new IllegalStateException(String.format("The result of the parsing operation could not be determined. "
				+ "The currently parsed element is '%s'.", XMLName.Element.RESULTS));
	}
}
