package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

/**
 * Implementation of the {@link AssociationRule} Interface.<br>
 * Represents an association rule for the apriori data mining algorithm.
 * 
 * @author Nicolas Bruch
 *
 */
class AssociationRuleImpl implements AssociationRule<TestedClass> {
	private final ItemSet<TestedClass> premise;
	private final ItemSet<TestedClass> conclusion;
	private final double ruleSupport;
	private final double confidence;

	/**
	 * 
	 * @param premise
	 *            {@link ItemSet} containing the premise of the rule.
	 * @param conclusion
	 *            {@link ItemSet} containing the conclusion of the rule.
	 * @param confidence
	 *            the confidence of the rule.
	 * @param ruleSupport
	 *            the support of the {@link ItemSet} that the rule is created
	 *            from.
	 */
	public AssociationRuleImpl(ItemSet<TestedClass> premise, ItemSet<TestedClass> conclusion, double confidence,
			double ruleSupport) {
		this.premise = premise;
		this.conclusion = conclusion;
		this.ruleSupport = ruleSupport;
		this.confidence = confidence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ItemSet<TestedClass> getPremise() {
		return premise;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ItemSet<TestedClass> getConclusion() {
		return conclusion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getSupport() {
		return ruleSupport;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getConfidence() {
		return confidence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (!(other instanceof AssociationRuleImpl)) {
			return false;
		} else {
			AssociationRuleImpl otherRule = (AssociationRuleImpl) other;
			return this.premise.equals(otherRule.premise) && this.conclusion.equals(otherRule.conclusion)
					&& this.ruleSupport == otherRule.ruleSupport && this.confidence == otherRule.confidence;
		}
	}
}
