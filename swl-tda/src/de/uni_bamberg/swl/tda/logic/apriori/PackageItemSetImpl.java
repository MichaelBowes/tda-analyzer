package de.uni_bamberg.swl.tda.logic.apriori;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a package. This class was created for consistent use of the
 * {@link AssociationRule} interface.
 * 
 * @author Nicolas Gross
 *
 */

class PackageItemSetImpl implements ItemSet<String> {

	private final Set<String> itemSet;

	/**
	 * Creates a new {@code PackageItemSet}.
	 * 
	 * @param pkgName
	 *            the name of the package.
	 */
	public PackageItemSetImpl(String pkgName) {
		this.itemSet = new HashSet<>();
		itemSet.add(pkgName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<String> iterator() {
		return itemSet.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> toJava() {
		return Collections.unmodifiableSet(itemSet);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return itemSet.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getSupport() {
		return -1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(String object) {
		return itemSet.contains(object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equalsSet(Set<String> set) {
		return itemSet.equals(set);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return itemSet == null ? 0 : itemSet.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (!(other instanceof PackageItemSetImpl)) {
			return false;
		} else {
			PackageItemSetImpl otherSet = (PackageItemSetImpl) other;
			return this.itemSet.equals(otherSet.itemSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return itemSet.stream().collect(Collectors.joining(", ", "{", "}"));
	}

}
