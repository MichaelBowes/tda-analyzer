package de.uni_bamberg.swl.tda.logic.apriori;

/**
 * Represents a association rule of an association mining algorithm.
 *
 * @author Florian Beetz
 */
public interface AssociationRule<T> {

	/**
	 * Returns the premise of this association rule.
	 *
	 * @return the premise {@link ItemSet}.
	 */
	ItemSet<T> getPremise();

	/**
	 * Returns the conclusion of this association rule.
	 *
	 * @return the conclusion {@link ItemSet}.
	 */
	ItemSet<T> getConclusion();

	/**
	 * Returns the support of this association rule.
	 * <p>
	 * The value lies between {@code 0} and {@code 1}.
	 *
	 * @return the support of the rule.
	 */
	double getSupport();

	/**
	 * Returns the confidence of this association rule.
	 * <p>
	 * The value lies between {@code 0} and {@code 1}.
	 *
	 * @return the confidence of the rule.
	 */
	double getConfidence();

}
