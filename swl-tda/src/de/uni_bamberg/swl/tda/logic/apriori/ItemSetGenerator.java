package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Acts as a generator for {@code ItemSet}s.
 * 
 * @author Nicolas Gross
 *
 */
class ItemSetGenerator {

	private final List<List<TestedClass>> allTransactions;
	private final Predicate<ItemSet<TestedClass>> minSupportReached;
	private final Predicate<ItemSet<TestedClass>> itemSetIsBelowMaxDistance;

	/**
	 * Creates a new {@code ItemSetGenerator}.
	 * 
	 * @param allTransactions
	 *            a list of lists containing the failed classes of each
	 *            {@link TestRun}.
	 * @param minSupportPerc
	 *            the minimum support an {@link ItemSet} has to fulfill.
	 * @param maxDistance
	 *            the maximum distance the classes of an {@link ItemSet} have to
	 *            fulfill.
	 */
	public ItemSetGenerator(List<List<TestedClass>> allTransactions, double minSupportPerc, int maxDistance) {
		this.allTransactions = allTransactions;
		this.minSupportReached = itemSet -> itemSet.getSupport() >= minSupportPerc;
		this.itemSetIsBelowMaxDistance = itemSet -> {
			for (TestedClass testedClass1 : itemSet) {
				for (TestedClass testedClass2 : itemSet) {
					if (testedClass1 != testedClass2 && calcDistance(testedClass1, testedClass2) > maxDistance) {
						return false;
					}
				}
			}
			return true;
		};
	}

	/**
	 * Generates a set of {@link ItemSet}s for a given list of transactions.
	 * 
	 * @return a set of the generated ItemSets.
	 */
	List<ItemSet<TestedClass>> generate() {
		List<ItemSet<TestedClass>> itemSets = generateItemSets();
		// itemSets = itemSets.stream().filter(set -> set.size() >
		// 1).collect(Collectors.toList());
		return removeTooFarDistances(itemSets);
	}

	/**
	 * Creates a new {@link ItemSet} from a given set of {@link TestedClass}es.
	 * 
	 * @param set
	 *            the set of {@link TestedClass}es.
	 * @return a new {@link ItemSet}.
	 */
	ItemSet<TestedClass> createItemSet(Set<TestedClass> set) {
		return new ItemSetImpl(set, calcSupport(set));
	}

	/**
	 * Generates a list of {@link ItemSet}s from a given list of transactions,
	 * unmindful of the distance between {@link TestedClass}es.
	 * 
	 * @return a set of {@link ItemSet}s.
	 */
	private List<ItemSet<TestedClass>> generateItemSets() {
		List<ItemSet<TestedClass>> itemSets = new LinkedList<>();
		List<ItemSet<TestedClass>> lastItemSets = generateOneItemSets();
		List<ItemSet<TestedClass>> nextItemSets;
		while (!(nextItemSets = generateNextItemSets(lastItemSets)).isEmpty()) {
			itemSets.addAll(lastItemSets);
			lastItemSets = nextItemSets;
		}
		itemSets.addAll(lastItemSets);
		return itemSets;
	}

	/**
	 * Generates {@link ItemSet}s of one element from a given list of
	 * transactions.
	 * 
	 * @return a set of {@link ItemSet}s.
	 */
	private List<ItemSet<TestedClass>> generateOneItemSets() {
		List<ItemSet<TestedClass>> oneSets = new LinkedList<>();
		for (List<TestedClass> classListOfRun : allTransactions) {
			for (TestedClass testedClass : classListOfRun) {
				Set<TestedClass> newSet = new HashSet<>();
				newSet.add(testedClass);
				// check weather set is already contained
				if (isItemSetNotGenerated(oneSets, newSet)) {
					oneSets.add(createItemSet(newSet));
				}
			}
		}
		return oneSets.stream().filter(minSupportReached).collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Generates {@link ItemSet}s of one element more than the last, from a
	 * given set of {@link ItemSet}s.
	 * 
	 * @param lastItemSets
	 *            the last set of {@link ItemSet}s.
	 * @return a set of {@link ItemSet}s.
	 */
	private List<ItemSet<TestedClass>> generateNextItemSets(List<ItemSet<TestedClass>> lastItemSets) {
		List<ItemSet<TestedClass>> nextSets = new LinkedList<>();
		for (ItemSet<TestedClass> itemSet : lastItemSets) {
			for (ItemSet<TestedClass> itemSetToCombine : lastItemSets) {

				// don't combine the same item sets
				if (itemSet.equals(itemSetToCombine)) {
					continue;
				}

				List<TestedClass> classesToAdd = new LinkedList<>();

				for (TestedClass classFromAnotherSet : itemSetToCombine) {
					if (classesToAdd.size() > 1) {
						// check whether item sets differ in more than one class
						break;
					} else if (!itemSet.contains(classFromAnotherSet)) {
						// add class to the list if it is not already part of
						// the item set
						classesToAdd.add(classFromAnotherSet);
					}
				}

				// if item sets only differ in one class, create a new item set
				// which is the union of both
				if (classesToAdd.size() == 1) {
					Set<TestedClass> newSet = new HashSet<>();
					newSet.add(classesToAdd.get(0));
					for (TestedClass alreadyPartOfItemSet : itemSet) {
						newSet.add(alreadyPartOfItemSet);
					}

					// // check whether set is already contained
					if (isItemSetNotGenerated(nextSets, newSet)) {
						nextSets.add(createItemSet(newSet));
					}
				}
			}
		}
		return nextSets.stream().filter(minSupportReached).collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Checks whether an {@code ItemSet} is not yet contained in an set of
	 * {@code ItemSet}s.
	 * 
	 * @param nextSets
	 *            the set of {@code ItemSet}s.
	 * @param newSet
	 *            the {@code ItemSet} which is checked.
	 * @return {@code false} if it is contained {@code false} otherwise.
	 */
	private boolean isItemSetNotGenerated(List<ItemSet<TestedClass>> nextSets, Set<TestedClass> newSet) {
		for (ItemSet<TestedClass> containedSet : nextSets) {
			if (containedSet.equalsSet(newSet)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Calculates the support for the {@link ItemSet}.
	 * 
	 * @param itemSet
	 *            the item set which support should be calculated.
	 * @return the support of the item set.
	 */
	private double calcSupport(Set<TestedClass> itemSet) {
		long containingTransactions = allTransactions.stream()
				.filter(failedClasses -> failedClasses.containsAll(itemSet)).count();

		return containingTransactions / (double) allTransactions.size();
	}

	/**
	 * Removes all {@link ItemSet}s from a set, which classes have a too high
	 * distance.
	 * 
	 * @param itemSets
	 *            the set of {@link ItemSet}s which should be checked.
	 * @return a set of {@link ItemSet}s.
	 */
	private List<ItemSet<TestedClass>> removeTooFarDistances(List<ItemSet<TestedClass>> itemSets) {
		return itemSets.stream().filter(itemSetIsBelowMaxDistance).collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Calculates the distance between two classes.
	 * <p>
	 * The minimum distance two classes could have, is 2. That's the case, if
	 * they are in the same package.
	 * 
	 * @param firstClass
	 *            the first {@link TestedClass}.
	 * @param secondClass
	 *            the second {@link TestedClass}.
	 * @return the distance between the two {@link TestedClass}es.
	 */
	private int calcDistance(TestedClass firstClass, TestedClass secondClass) {
		String[] firstPath = firstClass.getName().split("\\.");
		String[] secondPath = secondClass.getName().split("\\.");
		int minLength = Math.min(firstPath.length, secondPath.length);
		int distance = 0;
		int i = 0;
		while (i < minLength - 1) {
			if (!firstPath[i].equals(secondPath[i])) {
				distance += 2;
				i++;
				break;
			}
			i++;
		}
		distance += firstPath.length - i;
		distance += secondPath.length - i;
		return distance;
	}

}
