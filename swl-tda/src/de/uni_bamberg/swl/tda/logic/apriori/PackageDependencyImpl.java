package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.List;

/**
 * Represents a dependency between two packages.
 *
 * @author Nicolas Gross
 *
 */

class PackageDependencyImpl implements PackageDependency {

	private final String precondition;
	private final String implication;
	private final double meanSupport;
	private final double meanConfidence;
	private final List<AssociationRule<TestedClass>> strongRules;

	public PackageDependencyImpl(String precondition, String implication,
			List<AssociationRule<TestedClass>> strongRules, double meanSupport, double meanConfidence) {
		this.precondition = precondition;
		this.implication = implication;
		this.meanSupport = meanSupport;
		this.meanConfidence = meanConfidence;
		this.strongRules = strongRules;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ItemSet<String> getPremise() {
		return new PackageItemSetImpl(this.precondition);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ItemSet<String> getConclusion() {
		return new PackageItemSetImpl(this.implication);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getSupport() {
		return this.meanSupport;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getConfidence() {
		return this.meanConfidence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AssociationRule<TestedClass>> getAssociationRules() {
		return this.strongRules;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (!(other instanceof PackageDependencyImpl)) {
			return false;
		} else {
			PackageDependencyImpl otherDep = (PackageDependencyImpl) other;
			return this.precondition.equals(otherDep.precondition) && this.implication.equals(otherDep.implication)
					&& this.meanSupport == otherDep.meanSupport && this.meanConfidence == otherDep.meanConfidence
					&& this.strongRules.equals(otherDep.strongRules);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return precondition + " -> " + implication;
	}

}
