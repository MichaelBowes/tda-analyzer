package de.uni_bamberg.swl.tda.logic.apriori;

/**
 * An Exception for problems in the TDA's Apriori algorithm section.
 * 
 * @author Nicolas Gross
 *
 */

public class TdaAprioriException extends Exception {

	private static final long serialVersionUID = 3016704278523280072L;

	/**
	 * Constructs an {@code TdaAprioriException} with no detail message or
	 * cause.
	 */
	TdaAprioriException() {
		super();
	}

	/**
	 * Constructs an {@code TdaAprioriException} with the specified detail
	 * message.
	 *
	 * @param message
	 *            the detail message.
	 */
	TdaAprioriException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified cause.
	 * 
	 * @param cause
	 *            the cause.
	 */
	TdaAprioriException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs an {@code TdaAprioriException} with the specified detail
	 * message and cause.
	 * 
	 * @param message
	 *            the detail message.
	 * @param cause
	 *            the cause.
	 */
	TdaAprioriException(String message, Throwable cause) {
		super(message, cause);
	}
}
