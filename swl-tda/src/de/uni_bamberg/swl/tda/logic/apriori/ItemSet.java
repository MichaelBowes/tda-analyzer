package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.Set;

/**
 * Represents an item set.
 *
 * @author Florian Beetz
 */
public interface ItemSet<T> extends Iterable<T> {

	/**
	 * Returns the item set as a Java {@link Set}.
	 *
	 * @return java set.
	 */
	Set<T> toJava();

	/**
	 * Getter for the {@code ItemSet}'s size.
	 *
	 * @return the number of {@link TestedClass}es in the {@code ItemSet}.
	 */
	int size();

	/**
	 * Getter for the {@code ItemSet}'s support.
	 *
	 * @return the {@code ItemSet}'s support.
	 */
	double getSupport();

	/**
	 * Checks whether the {@code ItemSet} contains an object.
	 *
	 * @param object
	 *            the object which is checked.
	 * @return {@code true} if the object is present, otherwise {@code false}.
	 */
	boolean contains(T object);

	/**
	 * Checks whether an set equals the set of the {@code ItemSet}.
	 *
	 * @param set
	 *            the set which is checked against the set of the
	 *            {@code ItemSet}.
	 * @return {@code true}, if both sets are equal, otherwise {@code false}.
	 */
	boolean equalsSet(Set<T> set);

}
