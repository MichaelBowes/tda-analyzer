package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents an item set of the {@link Apriori} algorithm, which has items of
 * the type {@link TestedClass}.
 * 
 * @author Nicolas Gross
 *
 */

class ItemSetImpl implements ItemSet<TestedClass> {

	private final Set<TestedClass> itemSet;
	private final double support;

	/**
	 * Creates a new {@code ItemSetImpl}.
	 * 
	 * @param itemSet
	 *            the set of {@link TestedClass}es for the {@code ItemSetImpl}.
	 * @param support
	 *            the support of the {@code ItemSetImpl}.
	 */
	public ItemSetImpl(Set<TestedClass> itemSet, double support) {
		this.itemSet = itemSet;
		this.support = support;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<TestedClass> iterator() {
		return itemSet.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return itemSet.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(TestedClass testedClass) {
		return this.itemSet.contains(testedClass);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equalsSet(Set<TestedClass> set) {
		return itemSet.equals(set);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getSupport() {
		return this.support;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<TestedClass> toJava() {
		return Collections.unmodifiableSet(itemSet);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return itemSet == null ? 0 : itemSet.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (!(other instanceof ItemSetImpl)) {
			return false;
		} else {
			ItemSetImpl otherSet = (ItemSetImpl) other;
			return this.itemSet.equals(otherSet.itemSet) && this.support == otherSet.support;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return itemSet.stream().map(TestedClass::getName).collect(Collectors.joining(", ", "{", "}"));
	}

}
