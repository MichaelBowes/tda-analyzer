package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.*;

/**
 * Generator for {@link AssociationRuleImpl}.<br>
 * Needs an {@link ItemSetGenerator} for the creation of the {@link AssociationRuleImpl}.<br>
 * <br>
 *
 * @see <a href="www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/">Source</a>
 *
 * @author Nicolas Bruch
 */
class AssociationRuleGenerator {

    private final ItemSetGenerator itemSetGenerator;
    private final List<ItemSet<TestedClass>> itemSets;
    private final double minConfidence;
    private List<List<TestedClass>> subSetList = new ArrayList<>();
    private int listIndex = 0;

    /**
     * Creates a new {@code AssociationRuleGenerator}.
     *
     * @param generator     an {@link ItemSetGenerator} to generate Premise and Conclusion
     * @param itemSets      a {@link List} of {@link ItemSet} containing {@link TestedClass}es.
     * @param minConfidence the minimum confidence an {@link AssociationRule} must have, so that it is saved.
     */
    public AssociationRuleGenerator(ItemSetGenerator generator, List<ItemSet<TestedClass>> itemSets,
                                    double minConfidence) {
        this.itemSetGenerator = generator;
        this.itemSets = itemSets;
        this.minConfidence = minConfidence;
    }

    /**
     * Generates a list of {@link AssociationRule}<{@link TestedClass}>
     * from the {@link ItemSet}.
     *
     * @return a List containing all association rules for the ItemSets.
     */
    List<AssociationRule<TestedClass>> generate() {
        List<AssociationRule<TestedClass>> ruleList = new ArrayList<>();

        for (ItemSet<TestedClass> set : itemSets) {
            ruleList.addAll(generateAssociationRules(set));
        }
        return ruleList;
    }

    /**
     * Generates all association rules ({@link AssociationRuleImpl} ) for ONE {@link ItemSet}.
     *
     * @param set ItemSet for which the association rules shall be created.
     *            Can't be null.
     * @return A list of all {@code AssociationRules} for the {@code ItemSet}.
     */
    private List<AssociationRule<TestedClass>> generateAssociationRules(ItemSet<TestedClass> set) {
        List<AssociationRule<TestedClass>> rules = new ArrayList<>();
        generateSubsets(set);

        // loops through all subSets in the subSetList.
        for (List<TestedClass> a : subSetList) {
            // the Premise is stored in a temporary HashSet.
            Set<TestedClass> tempSetPre = new HashSet<>();
            tempSetPre.addAll(a);

            for (List<TestedClass> b : subSetList) {
                Set<TestedClass> tempSetConc = new HashSet<>();
                boolean containTest = false;
                if (a.size() + b.size() == set.size()) {
                    // checks if any of the TestedClass is contained in the
                    // subSet.
                    for (TestedClass c : b) {
                        if (tempSetPre.contains(c)) {
                            containTest = true;
                        }
                    }
                    // if no TestClass is contained in the subSet, the Set for
                    // Conclusion will be filled
                    // and the Rule will be created.
                    if (!containTest) {
                        tempSetConc.addAll(b);
                        ItemSet<TestedClass> tempSetPremise = itemSetGenerator.createItemSet(tempSetPre);
                        ItemSet<TestedClass> tempSetConclusion = itemSetGenerator.createItemSet(tempSetConc);
                        double confidence = set.getSupport() / tempSetPremise.getSupport();
                        if (confidence >= this.minConfidence) {
                            AssociationRuleImpl newRule = new AssociationRuleImpl(tempSetPremise, tempSetConclusion,
                                    confidence, set.getSupport());
                            rules.add(newRule);
                        }
                    }
                }

            }

        }
        subSetList.clear();
        listIndex = 0;
        return rules;
    }

    /**
     * Generates the subsets for a given {@link ItemSet}.
     *
     * @param itemset {@link ItemSet} that is not null;
     */
    private void generateSubsets(ItemSet<TestedClass> itemset) {
        List<TestedClass> templist = new ArrayList<>();
        int length = 0;
        for (TestedClass a : itemset) {
            templist.add(a);
            length++;
        }
        //calls subsetCombination of the ItemSet with the length 1 to n-1.
        for (int i = 1; i < length; i++) {
            subsetCombination(templist, length, i);
        }
    }

    /**
     * Method for creating all variations of subsets calls subsetCombinationUtil.<br>
     * This method starts the generation of the subsets and calls subsetCombinationUtil for the first time. <br>
     *
     * @param arr    {@link List} for which the subsets are to be created.
     * @param length length of the {@link List}.
     * @param size   Size of the subsets that are to be created.
     */
    private void subsetCombination(List<TestedClass> arr, int length, int size) {
        // A temporary array to store all combination one by one
        TestedClass data[] = new TestedClass[size];
        subsetCombinationUtil(arr, data, 0, length - 1, 0, size);
    }

    /**
     * Method that is called recursively by itself in order to create the subsets.<br>
     * This method will call itself until the index has reached the size of the {@link List}.<br>
     *
     * @param arr   The {@link List} for which the subsets are to be created.
     * @param data  {@link List} for storing the generated subsets.
     * @param start variable for determining the start point in the {@link List}.
     * @param end   the index for the end of the {@link List}.
     * @param index variable for determining which index in the list needs to be accessed.
     * @param size  The size of the subsets that are to be created.
     */
    private void subsetCombinationUtil(List<TestedClass> arr, TestedClass data[], int start, int end, int index, int size) {
        // Current combination is ready to be stored in temp.
        if (index == size) {
            subSetList.add(new ArrayList<>());
            for (TestedClass a : data) {
                subSetList.get(listIndex).add(a);
            }
            listIndex++;
            return;
        }

        // replace index with all possible elements. The condition
        // "end-i+1 >= r-index" makes sure that including one element
        // at index will make a combination with remaining elements
        // at remaining positions
        for (int i = start; i <= end && end - i + 1 >= size - index; i++) {
            data[index] = arr.get(i);
            subsetCombinationUtil(arr, data, i + 1, end, index + 1, size);
        }
    }

}
