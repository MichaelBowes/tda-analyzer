package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestRun;
import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Represents the procedure of the Apriori algorithm used in TDA.
 *
 * @author Nicolas Bruch
 * @author Nicolas Gross
 * @author Florian Beetz
 * @author Christian Hild
 * @author Michael Bowes
 */

public class Apriori {

	private static final Logger LOGGER = Logger.getLogger(Apriori.class.getName());

	private final List<TestRun> testRuns;

	private double minSupportPerc = 0.6;
	private double minConfidencePerc = 0.7;

	private int maxDistance = 5;
	private List<ItemSet<TestedClass>> frequentItemSets = new LinkedList<>();
	private List<AssociationRule<TestedClass>> strongRules = new LinkedList<>();
	private List<PackageDependency> packageDependencies = new LinkedList<>();

	/**
	 * Creates a new {@code Apriori} object with default values.
	 * This means a minimum support of 0.6 and a minimum confidence of 0.7.
	 *
	 * @param testRuns the {@link TestRun}s which should be analyzed.
	 */
	public Apriori(List<TestRun> testRuns) {
		this.testRuns = testRuns;
	}

	/**
	 * Creates a new {@code Apriori} object with .
	 *
	 * @param testRuns          the {@link TestRun}s which should be analyzed.
	 * @param minSupportPerc    sets the minimal support - must be a percentage represented as
	 *                          a double between 0 and 1
	 * @param minConfidencePerc sets the minimal confidence - must be a percentage represented
	 *                          as a double between 0 and 1
	 * @throws TdaAprioriException if parameters are invalid.
	 */
	public Apriori(List<TestRun> testRuns, double minSupportPerc, double minConfidencePerc) throws TdaAprioriException {
		this.setMinSupport(minSupportPerc);
		this.setMinConfidence(minConfidencePerc);
		this.testRuns = testRuns;
	}

	/**
	 * Analyzes the dependencies between {@link TestedClass}es of the given
	 * {@link TestRun}s.
	 */
	public void analyze() {
		LOGGER.fine("Running apriori algorithm on " + testRuns.size() + " transactions");
		long start = System.currentTimeMillis();
		List<List<TestedClass>> failedClasses = filterFailurePerc(testRuns);

		ItemSetGenerator itemSetGenerator = new ItemSetGenerator(failedClasses, minSupportPerc, maxDistance);
		frequentItemSets = itemSetGenerator.generate();
		LOGGER.finer("Generated " + frequentItemSets.size() + " item sets");

		AssociationRuleGenerator associationRuleGenerator = new AssociationRuleGenerator(itemSetGenerator,
				frequentItemSets, minConfidencePerc);
		strongRules = associationRuleGenerator.generate();
		LOGGER.finer("Generated " + strongRules.size() + " association rules");

		PackageDependencyGenerator generator = new PackageDependencyGenerator(strongRules);
		packageDependencies = generator.generatePackageDependencies();
		LOGGER.finer("Grouped rules into " + packageDependencies.size() + " package rules");

		LOGGER.fine("apriori algorithm took " + (System.currentTimeMillis() - start) + "ms");
	}

	/**
	 * Takes a list of {@link TestRun}s and sorts out the {@link TestedClass}es
	 * with a failure percentage below 15%.
	 *
	 * @param testRuns the {@link TestRun}s, which {@link TestedClass}es should be
	 *                 sorted out
	 * @return a list of lists containing the {@link TestedClass}es with failure
	 * percentage >= 15% of each {@link TestRun}.
	 */
	List<List<TestedClass>> filterFailurePerc(List<TestRun> testRuns) {
		List<List<TestedClass>> filteredClassesPerRun = new LinkedList<>();
		Predicate<TestedClass> failurePercAtLeast15 = testedClass -> testedClass.getFailurePercentage() >= 15;
		for (TestRun testRun : testRuns) {
			filteredClassesPerRun.add(testRun.getClassList().stream().filter(failurePercAtLeast15)
					.collect(Collectors.toCollection(LinkedList::new)));
		}
		return filteredClassesPerRun;
	}

	/**
	 * Getter for the {@code Apriori}'s minimum support.
	 *
	 * @return the {@code Apriori}'s minimum support.
	 */
	public double getMinSupport() {
		return minSupportPerc;
	}

	/**
	 * Setter for the {@code Apriori}'s minimum support.
	 *
	 * @param minSupport the {@code Apriori}'s minimum support.
	 * @throws TdaAprioriException if minSupport is less than 0 or bigger than 1.
	 */
	public void setMinSupport(double minSupport) throws TdaAprioriException {
		if (minSupport <= 0 || minSupport >= 1) {
			throw new TdaAprioriException("The minimum support has to be at least 0 and can't be greater than 1.");
		}
		this.minSupportPerc = minSupport;
	}

	/**
	 * Getter for the {@code Apriori}'s minimum confidence.
	 *
	 * @return the {@code Apriori}'s minimum confidence.
	 */
	public double getMinConfidence() {
		return minConfidencePerc;
	}

	/**
	 * Setter for the {@code Apriori}'s minimum confidence.
	 *
	 * @param minConfidence the {@code Apriori}'s minimum confidence.
	 * @throws TdaAprioriException if minConfidence is less than 0 or bigger than 1.
	 */
	public void setMinConfidence(double minConfidence) throws TdaAprioriException {
		if (minConfidence <= 0 || minConfidence >= 1) {
			throw new TdaAprioriException("The minimum confidence has to be at least 0 and can't be greater than 1.");
		}
		this.minConfidencePerc = minConfidence;
	}

	/**
	 * Getter for the {@code Apriori}'s maximal distance between two classes.
	 *
	 * @return the {@code Apriori}'s maximal distance between two classes.
	 */
	public int getMaxDistance() {
		return maxDistance;
	}

	/**
	 * Setter for the {@code Apriori}'s maximum distance between two classes.
	 *
	 * @param maxDistance the {@code Apriori}'s maximum distance between two classes.
	 * @throws TdaAprioriException if maxDistance is less than 0.
	 */
	public void setMaxDistance(int maxDistance) throws TdaAprioriException {
		if (maxDistance < 0) {
			throw new TdaAprioriException("The maximum distance between two classes has to be at least 0.");
		}
		this.maxDistance = maxDistance;
	}

	/**
	 * Getter for the {@code Apriori}'s frequent {@link ItemSet}s.
	 *
	 * @return the {@code Apriori}'s frequent {@link ItemSet}s.
	 */
	public List<ItemSet<TestedClass>> getFrequentItemSets() {
		return frequentItemSets;
	}

	/**
	 * Getter for the {@code Apriori}'s strong {@link AssociationRule}s.
	 *
	 * @return the {@code Apriori}'s strong {@link AssociationRule}s.
	 */
	public List<AssociationRule<TestedClass>> getStrongRules() {
		return strongRules;
	}

	/**
	 * Getter for the {@link PackageDependency}s.
	 *
	 * @return a list containing the {@link PackageDependency}s.
	 */
	public List<PackageDependency> getPackageDependencies() {
		return this.packageDependencies;
	}

}
