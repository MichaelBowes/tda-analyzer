package de.uni_bamberg.swl.tda.logic.apriori;

import java.util.List;

import de.uni_bamberg.swl.tda.logic.TestedClass;

/**
 * Represents a dependency between two packages.
 * 
 * @author Nicolas Gross
 *
 */

public interface PackageDependency extends AssociationRule<String> {

	/**
	 * Getter for the list of all rules included in the dependency.
	 *
	 * @return the list of all included rules.
	 */
	List<AssociationRule<TestedClass>> getAssociationRules();

}
