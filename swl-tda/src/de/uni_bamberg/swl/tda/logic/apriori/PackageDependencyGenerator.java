package de.uni_bamberg.swl.tda.logic.apriori;

import de.uni_bamberg.swl.tda.logic.TestedClass;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Generates {@link PackageDependency}s from {@link AssociationRule}s.
 *
 * @author Nicolas Gross
 *
 */

class PackageDependencyGenerator {

	private final List<AssociationRule<TestedClass>> strongRules;

	PackageDependencyGenerator(List<AssociationRule<TestedClass>> strongRules) {
		this.strongRules = strongRules;
	}

	/**
	 * Generates a list of {@link PackageDependency}s from the list of
	 * {@link AssociationRule}s.
	 *
	 * @return a list of {@link PackageDependency}s.
	 */
	List<PackageDependency> generatePackageDependencies() {
		List<AssociationRule<TestedClass>> rulesLeft = new LinkedList<>(strongRules);
		List<PackageDependency> depList = new LinkedList<>();
		while (!rulesLeft.isEmpty()) {
			AssociationRule<TestedClass> rule = rulesLeft.get(0);
			String precondition = lowestCommonPackage(rule.getPremise());
			String implication = lowestCommonPackage(rule.getConclusion());
			List<AssociationRule<TestedClass>> pkgDepRules = new LinkedList<>();
			pkgDepRules.add(rule);
			rulesLeft.remove(0);
			Iterator<AssociationRule<TestedClass>> iterator = rulesLeft.iterator();
			while (iterator.hasNext()) {
				AssociationRule<TestedClass> otherRule = iterator.next();
				String tmpPre = lowestCommonPackage(otherRule.getPremise());
				if (precondition.equals(tmpPre)) {
					String tmpImpl = lowestCommonPackage(otherRule.getConclusion());
					if (implication.equals(tmpImpl)) {
						pkgDepRules.add(otherRule);
						iterator.remove();
					}
				}
			}
			PackageDependency pkgDep = new PackageDependencyImpl(precondition, implication, pkgDepRules,
					calcMeanSupport(pkgDepRules), calcMeanConfidence(pkgDepRules));
			depList.add(pkgDep);
		}
		return depList;
	}

	/**
	 * Finds the lowest common package of an {@link ItemSet}.
	 *
	 * @param itemSet
	 *            the {@link ItemSet} in which the method searches.
	 * @return lowest common package.
	 */
	private String lowestCommonPackage(ItemSet<TestedClass> itemSet) {
		StringBuilder commonPackage = new StringBuilder();

		String[] firstPackageSeq;

		assert itemSet.size() > 0; // should always be true, itemSets with one element are not generated
		firstPackageSeq = itemSet.iterator().next().getName().split("\\.");

		boolean noDifferenceFound = true;
		for (int i = 0; noDifferenceFound && i < firstPackageSeq.length - 1; i++) {
			for (TestedClass testedClass : itemSet) {
				if (!testedClass.getName().split("\\.")[i].equals(firstPackageSeq[i])) {
					noDifferenceFound = false;
					break;
				}
			}

			if (noDifferenceFound) {
				commonPackage.append(firstPackageSeq[i]);
				commonPackage.append(".");
			}
		}

		commonPackage.deleteCharAt(commonPackage.length() - 1);

		return commonPackage.toString();
	}

	/**
	 * Calculates the mean support over a list of {@link AssociationRule}s.
	 *
	 * @param rules
	 *            the list of {@link AssociationRule}s.
	 * @return the mean support.
	 */
	private double calcMeanSupport(List<AssociationRule<TestedClass>> rules) {
		return rules.stream().mapToDouble(AssociationRule::getSupport).sum() / rules.size();
	}

	/**
	 * Calculates the mean confidence over a list of {@link AssociationRule}s.
	 *
	 * @param rules
	 *            the list of {@link AssociationRule}s.
	 * @return the mean confidence.
	 */
	private double calcMeanConfidence(List<AssociationRule<TestedClass>> rules) {
		return rules.stream().mapToDouble(AssociationRule::getConfidence).sum() / rules.size();
	}

}
